using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddcameraShake : MonoBehaviour
{
	public void AddShake(float intensity)
	{
		CameraShaker.Shake(intensity);
	}
}
