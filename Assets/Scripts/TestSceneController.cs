using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestSceneController : MonoBehaviour
{
	[SerializeField]
	public List<EnemySpawner> EnemySpawners;

	[SerializeField]
	public PlayerManager PlayerManager;

    [SerializeField]
    public List<PlayerCharacter> CharacterList;

    public bool SpawnEnemies = false;



    private void Start()
    {
        PlayerManager.OnPlayerAdded.AddListener(NewPlayerJoined);

        UpdateSpawning();
    }

    private void UpdateSpawning()
    {
        foreach (var item in EnemySpawners)
        {
            item.SpawnEnemies = SpawnEnemies;
        }
    }

    private void NewPlayerJoined(OnNewPlayerAddedArgs arg0)
    {
        CharacterList.Add(arg0.character.GetComponent<PlayerCharacter>());
    }

    private void Update()
    {
    }

    private void OnGUI()
    {
        GUILayout.Space(150);

        if(GUILayout.Button(SpawnEnemies?"Stop Spawning":"Start Spawning"))
        {
            SpawnEnemies = !SpawnEnemies;
            UpdateSpawning();
        }

        for(int i = 0; i < CharacterList.Count; i++)
        {
            if(GUILayout.Button("Player"+(i+1)+ " Kill"))
            {
                var health = CharacterList[i].GetComponent<EntityStats>();
                if(health != null)
                {
                    health.GetHit(this, new OnHitEventArgs() { Damage = health.MaxHealth, Origin = this.transform.position, PlayerBullet = !health.IsPlayer });
                }
            }
        }
    }
}
