using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerSpawn : MonoBehaviour
{

    [SerializeField]
    private List<Transform> spawnPoints;

    public UnityEvent OnRespawn = new UnityEvent();

    public void RespawnPlayers(List<PlayerCharacter> characters)
    {
        foreach (var body in FindObjectsOfType<RagdollBody>())
        {
            if(body.GetComponent<PlayerCharacter>() == null)
                Destroy(body.gameObject);
        }
        for (int i = 0; i < characters.Count; i++)
        {
            SpawnPlayer(characters[i], i);
        }
        OnRespawn.Invoke();
    }

    public void SpawnPlayer(PlayerCharacter character, int index = 0)
    {
        Vector3 position = this.transform.position;
        if (spawnPoints.Count > 0)
        {
            int j = index < spawnPoints.Count ? index : 0;
            position = spawnPoints[j].position;
        }
        Debug.Log($"Respawn Location = {position}");
        character.Respawn(position);
    }
}
