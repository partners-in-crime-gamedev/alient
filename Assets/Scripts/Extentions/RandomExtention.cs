using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomExtention
{
    public static float NormalRange(float minInclusive, float maxInclusive)
    {
        List<float> list = new List<float>();
        list.Add(Random.Range(minInclusive, maxInclusive));
        list.Add(Random.Range(minInclusive, maxInclusive));
        list.Add(Random.Range(minInclusive, maxInclusive));
        list.Sort();
        return list[1];
    }
    public static int NormalRange(int minInclusive, int maxExclusive)
    {
        List<int> list = new List<int>();
        list.Add(Random.Range(minInclusive, maxExclusive));
        list.Add(Random.Range(minInclusive, maxExclusive));
        list.Add(Random.Range(minInclusive, maxExclusive));
        list.Sort();
        return list[1];
    }
}
