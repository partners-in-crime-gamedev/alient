using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ListExtention
{
    public static TType GetRandom<TType>(this List<TType> list)
    {
        return list[Random.Range(0, list.Count)];
    }
}