using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

public class OnPlayerEnterTrigger : MonoBehaviour
{
    public LayerMask playerMask;
    public UnityEvent OnTrigger = new UnityEvent();

    [FormerlySerializedAs("DisableOnTrigger")]public bool DisableOnTrigger = true;
    private PlayerManager playerManager;
    private HashSet<GameObject> players = new HashSet<GameObject>();
    public bool AnyPlayer;

    // Start is called before the first frame update

    void Start()
    {
        playerManager = FindObjectOfType<PlayerManager>();
    }

    // Update is called once per frame

    void Update()
    {

    }

    void OnEnable()
    {
        players.Clear();
    }

    void OnTriggerEnter(Collider other)
    {
        if (playerMask == (playerMask | (1 << other.gameObject.layer)))
        {
            other.gameObject.GetComponent<EntityStats>().OnDeath.AddListener(RemoveAfterDeath);
            players.Add(other.gameObject);
            if (AnyPlayer || players.Count >= playerManager.AliveCount)
            {
                OnTrigger.Invoke();
                if (DisableOnTrigger)
                {
                    gameObject.SetActive(false);
                }
            }
        }
    }

    private void RemoveAfterDeath(object sender, OnDeathEventArgs args)
    {
        players.Remove(args.MurderVictim);
    }

    void OnTriggerExit(Collider other)
    {
        if (!AnyPlayer && playerMask == (playerMask | (1 << other.gameObject.layer)))
        {
            other.gameObject.GetComponent<EntityStats>().OnDeath.RemoveListener(RemoveAfterDeath);
            players.Remove(other.gameObject);
        }
    }
}
