using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/*
* Created by webik150
*/

public class InvokeAfterTime : MonoBehaviour
{
    public float Time = 1f;
    public bool TriggerOnEnable = false;
    public UnityEvent OnTimeElapsed = new UnityEvent();
    // Start is called before the first frame update
    void OnEnable()
    {
        if (TriggerOnEnable)
        {
            Trigger();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Trigger()
    {
        StartCoroutine(WaitForTime());
    }

    private IEnumerator WaitForTime()
    {
        yield return new WaitForSeconds(Time);
        OnTimeElapsed.Invoke();
    }
}
