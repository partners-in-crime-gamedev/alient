using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

[RequireComponent(typeof(CinemachineVirtualCamera))]
public class CameraShaker : MonoBehaviour
{
	private static CinemachineVirtualCamera cam;
	CinemachineBasicMultiChannelPerlin noise;

    private static CameraShaker _instance;

    public static CameraShaker Instance
    {
        get => _instance;
    }

    [SerializeField]private float currentShake;

    [SerializeField] private float recoverySpeed = 1f;
    // Start is called before the first frame update
    void Awake()
    {
        _instance = this;
        cam = GetComponent<CinemachineVirtualCamera>();
        noise = cam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
	    if (currentShake > 0f)
	    {
		    currentShake = Mathf.Lerp(currentShake, 0, Time.fixedDeltaTime / recoverySpeed);
            currentShake = Mathf.Clamp(currentShake, 0, 3f);
		    noise.m_AmplitudeGain = currentShake;
        }
    }

    public static void Shake(float intensity = 1.0f)
    {
        if (Instance)
        {
            Instance.currentShake += intensity;
        }
    }
}
