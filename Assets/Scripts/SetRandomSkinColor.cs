using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetRandomSkinColor : MonoBehaviour
{
    [SerializeField] private Material[] _skinMaterials;

    [SerializeField] private Renderer _renderer;

    [SerializeField] private int _materialId;
    // Start is called before the first frame update
    void Start()
    {
        var m = _renderer.materials;
        m[_materialId] = _skinMaterials[Random.Range(0, _skinMaterials.Length)];
        _renderer.materials = m;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
