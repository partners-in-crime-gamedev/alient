using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Assertions;

public class LockableDoors : MonoBehaviour
{
    [Serializable]
    public enum DoorStates
    {
        Unlocked = 0,
        Locked = 1,
        EnemiesOnly = 2,
        Off = 3
    }
    [Serializable]
    private class RendererMatPair
    {
        public Renderer Renderer;
        public int Index;
    }

    [SerializeField] private Animator _animator;
    [SerializeField] private Material _unlockedMaterial;
    [SerializeField] private Material _lockedMaterial;
    [SerializeField] private Material _enemiesOnlyMaterial;
    [SerializeField] private Material _offMaterial;
    [SerializeField] private RendererMatPair[] _renderers;
    [SerializeField] private Collider _doorCollider;
    [SerializeField] private NavMeshObstacle _navMeshObstacle;
    [SerializeField] private GameObject _bars;

    public DoorStates DoorState;
    private HashSet<GameObject> _entities = new HashSet<GameObject>();
    private Dictionary<Renderer, Material[]> mats = new Dictionary<Renderer, Material[]>();
   
    private FMOD.Studio.EventInstance sfx;
    private FMOD.Studio.EventInstance sfxFf;
    private bool openDoor;
    private bool forceField;
    private PauseManager pauseManager;
    private bool stoppedForceFieldSFX;

    private DoorStates _initialState;

    // Start is called before the first frame update
    void Start()
    {
        _initialState = DoorState;
        Assert.IsNotNull(_animator);
        foreach (var rendererPair in _renderers)
        {

            mats[rendererPair.Renderer] = rendererPair.Renderer.materials;
            rendererPair.Renderer.sharedMaterials = mats[rendererPair.Renderer];
        }

        openDoor = false;
        forceField = false;
        stoppedForceFieldSFX = false;
        pauseManager = FindObjectOfType<PauseManager>();
        pauseManager.OnPauseEvent.AddListener(StopForceField);
        pauseManager.OnUnPauseEvent.AddListener(RestartForceField);
    }

    // Update is called once per frame
    void Update()
    {
        if (_entities.Count > 0)
        {
            _animator.SetBool("open", true);
            PlaySFXOpenDoor(openDoor, true);
            openDoor = true;
            if (DoorState == DoorStates.EnemiesOnly)
            {
                SFXForceField(true, forceField);
                forceField = true;
            }
        }
        else
        {
            _animator.SetBool("open", false);
            PlaySFXOpenDoor(openDoor, false);
            openDoor = false;
            StopForceField();
            forceField = false;
        }
        Material selectedMat;

        switch (DoorState)
        {
            case DoorStates.Unlocked:
                selectedMat = _unlockedMaterial;
                _doorCollider.gameObject.layer = 0;
                if (_entities.Count > 0)
                {
                    _doorCollider.enabled = false;
                    _navMeshObstacle.enabled = false;
                }
                else
                {
                    _doorCollider.enabled = true;
                    _navMeshObstacle.enabled = false;
                }
                _bars.SetActive(false);
                break;
            case DoorStates.Locked:
                _entities.Clear();
                _doorCollider.gameObject.layer = 0;
                _doorCollider.enabled = true;
                _navMeshObstacle.enabled = true;
                selectedMat = _lockedMaterial;
                _bars.SetActive(true);
                break;
            case DoorStates.EnemiesOnly:
                _doorCollider.enabled = true;
                _navMeshObstacle.enabled = false;
                if (_entities.Count > 0)
                {
                    _doorCollider.gameObject.layer = LayerMask.NameToLayer("Doors");
                }
                else
                {
                    _doorCollider.gameObject.layer = 0;
                }
                selectedMat = _enemiesOnlyMaterial;
                _bars.SetActive(true);
                break;
            case DoorStates.Off:
                _doorCollider.enabled = true;
                _navMeshObstacle.enabled = false;
                if (_entities.Count > 0)
                {
                    _doorCollider.gameObject.layer = LayerMask.NameToLayer("Doors");
                }
                else
                {
                    _doorCollider.gameObject.layer = 0;
                }
                selectedMat = _offMaterial;
                _bars.SetActive(false);
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

        foreach (var rendererPair in _renderers)
        {
            mats[rendererPair.Renderer][rendererPair.Index] = selectedMat;
            rendererPair.Renderer.sharedMaterials = mats[rendererPair.Renderer];
        }
    }

    void StopForceField()
    {
        if (forceField)
        {
            StopFf();
        }
    }

    void RestartForceField()
    {
        if (forceField)
        {
            StartFf();
        }
    }

    void StartFf()
    {
        sfxFf = FMODUnity.RuntimeManager.CreateInstance("event:/SFX/forcefield");
        sfxFf.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject.transform));
        sfxFf.start();
        sfxFf.release();
    }

    void StopFf()
    {
        sfxFf.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
    }

    void SFXForceField(bool active, bool wasFf)
    {
        if (active && wasFf != active)
        {
            StartFf();
        }
        else if (!active && wasFf != active)
        {
            StopFf();
        }
    }

    void PlaySFXOpenDoor(bool status1, bool status2) 
    {
        if (status1 != status2) 
        {
            sfx = FMODUnity.RuntimeManager.CreateInstance("event:/SFX/door-open");
            sfx.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject.transform));
            sfx.start();
            sfx.release();
        }
    }

    void OnTriggerStay(Collider col)
    {
        switch (DoorState)
        {
            case DoorStates.Unlocked:
                _entities.Add(col.transform.root.gameObject);
                break;
            case DoorStates.Locked:
                break;
            case DoorStates.EnemiesOnly:
                if (col.gameObject.layer == LayerMask.NameToLayer("Enemy"))
                {
	                if (!_entities.Contains(col.transform.gameObject))
	                {
		                _entities.Add(col.transform.gameObject);
                        col.GetComponent<EntityStats>().OnDeath.AddListener((sender,args) =>
                        {
	                        _entities.Remove(col.transform.gameObject);
                        });
	                }
                }
                break;
            case DoorStates.Off:
                if (col.gameObject.layer == LayerMask.NameToLayer("Enemy"))
                {
                    if (!_entities.Contains(col.transform.gameObject))
                    {
                        _entities.Add(col.transform.gameObject);
                        col.GetComponent<EntityStats>().OnDeath.AddListener((sender, args) =>
                        {
                            _entities.Remove(col.transform.gameObject);
                        });
                    }
                }
                break;
        }

    }

    void OnTriggerExit(Collider col)
    {
        _entities.Remove(col.transform.gameObject);
    }

    public void UnlockDoors()
    {
	    DoorState = DoorStates.Unlocked;
        _entities.Clear();
    }
    public void MakeEnemiesOnlyDoors()
    {
	    DoorState = DoorStates.EnemiesOnly;
	    _entities.Clear();
    }
    public void LockDoors()
    {
	    DoorState = DoorStates.Locked;
	    _entities.Clear();
    }
    public void TurnOffDoors()
    {
	    DoorState = DoorStates.Off;
	    _entities.Clear();
    }

    public void ResetToInitialState()
    {
        DoorState = _initialState;
        _entities.Clear();
    }

}
