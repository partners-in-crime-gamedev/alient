using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideObjectOnTriggerEnter : MonoBehaviour
{

    [SerializeField] GameObject _objectToHide;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        _objectToHide.SetActive(false);
    }
}
