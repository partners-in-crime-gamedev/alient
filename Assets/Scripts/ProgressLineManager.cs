using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
* Created by webik150
*/

public class ProgressLineManager : MonoBehaviour
{
    [SerializeField] private LineRenderer _shieldLine;
    [SerializeField] private LineRenderer _shieldLineOff;
    [SerializeField] private LineRenderer _blinkLine;
    [SerializeField] private LineRenderer _blinkLineOff;
    [SerializeField] private LineRenderer _plasmaLine;
    [SerializeField] private LineRenderer _plasmaLineOff;

    private PlayerCharacter _player;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (_player == null)
        {
            _player = FindObjectOfType<PlayerCharacter>();
            _blinkLine.gameObject.SetActive(false);
            _blinkLineOff.gameObject.SetActive(true);
            _shieldLine.gameObject.SetActive(false);
            _shieldLineOff.gameObject.SetActive(true);
            _plasmaLine.gameObject.SetActive(false);
            _plasmaLineOff.gameObject.SetActive(true);
        }
        else
        {
            if (_player.HasBlink)
            {
                _blinkLine.gameObject.SetActive(true);    
                _blinkLineOff.gameObject.SetActive(false);    
            }
            else
            {
                _blinkLine.gameObject.SetActive(false);
                _blinkLineOff.gameObject.SetActive(true);
            }
            if (_player.HasShield)
            {
                _shieldLine.gameObject.SetActive(true);    
                _shieldLineOff.gameObject.SetActive(false);    
            }
            else
            {
                _shieldLine.gameObject.SetActive(false);
                _shieldLineOff.gameObject.SetActive(true);
            }
            if (_player.HasPlasma)
            {
                _plasmaLine.gameObject.SetActive(true);    
                _plasmaLineOff.gameObject.SetActive(false);    
            }
            else
            {
                _plasmaLine.gameObject.SetActive(false);
                _plasmaLineOff.gameObject.SetActive(true);
            }
        }
    }
}
