using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public sealed class OnHitEventArgs
{
    public bool PlayerBullet { get; internal set; }
    public float Damage { get; internal set; }
    public Vector3 Origin { get; set; }
    public float Force { get; set; }
}

public class OnHitScript : MonoBehaviour
{
    public UnityEvent<object, OnHitEventArgs> OnHit = new UnityEvent<object, OnHitEventArgs>();

    internal void InvokeOnHit(OnHitEventArgs args)
    {
        OnHit?.Invoke(this.gameObject, args);
    }
}
