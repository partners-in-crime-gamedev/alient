using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BlinkScript : MonoBehaviour
{


    public float BlinkDistance;
    public float BlinkTime;
    public float BlinkCooldown;
    public float BlinkConfusionDuration;

    [SerializeField]
    public TeleportIndicatorScript Indicator;

    [SerializeField] private GameObject _blinkStartParticles;
    [SerializeField] private GameObject _blinkEndParticles;

    public float CooldownProgress => 1f - _counter / BlinkCooldown;

    private float _counter;
    private CharacterController _characterController;

    private bool canBlink = true;
    private Vector3 _aimDirection;

    private FMOD.Studio.EventInstance sfx;
    private bool _indicatorHasGround;

    public UnityEvent<GameObject, float> OnBlinkEvent = new();

    private void Start()
    {
        _characterController = transform.parent.GetComponent<CharacterController>();
        //Indicator.SetDisplaying(true);
    }

    private void Update()
    {
        if (_counter > 0)
            _counter -= Time.deltaTime;

        if (!canBlink && _counter <= 0)
        {
            Indicator.SetEmission(1f);
            canBlink = true;
        }


        UpdateIndicatorPosition(_aimDirection);
    }

    private void UpdateIndicatorPosition(Vector3 aimDir)
    {
        float dist = BlinkDistance + _characterController.radius;

        LayerMask layerMask = LayerMask.GetMask("Enemy", "Player", "PlayerProjectiles", "EnemyProjectiles", "Ragdoll", "Ignore Raycast");

        RaycastHit hit;

        if (Physics.Raycast(transform.position + Vector3.up * 0.5f, aimDir, out hit, dist, ~layerMask, QueryTriggerInteraction.Ignore))
        {
            Debug.DrawRay(transform.position + Vector3.up * 0.5f, aimDir * hit.distance, Color.yellow);
            //Debug.Log("Did Hit");

            Indicator.transform.position = hit.point - Vector3.up * 0.5f - aimDir * _characterController.radius;
        }
        else
        {
            Debug.DrawRay(transform.position + Vector3.up * 0.5f, aimDir * dist, Color.white);
            //Debug.Log("Did not Hit");
            Indicator.transform.position = transform.position + aimDir * BlinkDistance;
        }
        _indicatorHasGround = Physics.Raycast(Indicator.transform.position + Vector3.up * 0.5f, Vector3.down, 3, LayerMask.GetMask("Default", "Ground"),
                QueryTriggerInteraction.Ignore);
        if (!_indicatorHasGround)
        {
            if (Physics.Raycast(transform.position + Vector3.up * 0.125f, aimDir, out hit, dist, ~layerMask, QueryTriggerInteraction.Ignore))
            {
                Indicator.transform.position = hit.point - Vector3.up * 0.125f - aimDir * _characterController.radius;
                _indicatorHasGround = true;
            }
        }
    }

    public void UpdateAim(Vector3 aimDir)
    {
        //Debug.Log("aimDir="+aimDir);
        _aimDirection = aimDir;
        transform.rotation = Quaternion.LookRotation(aimDir);
    }

    public bool CanBlink()
    {
        return _counter <= 0 && _indicatorHasGround;
    }

    public void Blink()
    {
        if (!CanBlink())
        {
            Indicator.SetDisplaying(false);
            return;
        }

        StartCoroutine(BlinkMovePlayer());
        
    }

    public IEnumerator BlinkMovePlayer()
    {
        var particles = Instantiate(_blinkStartParticles, _characterController.transform.position, Quaternion.identity);
        particles.SetActive(true);
        OnBlinkEvent.Invoke(particles, BlinkConfusionDuration);
        yield return null;
        _characterController.enabled = false;
        _characterController.transform.position = Indicator.transform.position;
        _characterController.enabled = true;
        Instantiate(_blinkEndParticles, _characterController.transform.position, Quaternion.identity).SetActive(true);
        _counter = BlinkCooldown;
        canBlink = false;
        Indicator.SetEmission(0.2f);
        Indicator.SetDisplaying(false);
        PlaySFX();
    }

    public void SetUp()
    {
        Indicator.SetDisplaying(true);
        if (canBlink)
            Indicator.SetEmission(1);
    }

    void PlaySFX()
    {
        sfx = FMODUnity.RuntimeManager.CreateInstance("event:/SFX/blink");
        sfx.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject.transform));
        sfx.start();
        sfx.release();
    }
}
