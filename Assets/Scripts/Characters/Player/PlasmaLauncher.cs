using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlasmaLauncher : MonoBehaviour
{
    [SerializeField]
    private BoomProjectileScript Projectile;
    [SerializeField]
    private Transform Barrel;
    [SerializeField]
    public float Cooldown;
    [SerializeField]
    private float ProjectileForce = 1000;

    public float BoomDamage;

    private float _counter = 0f;

    public float CooldownProgress => 1f - _counter / Cooldown;

    public void Fire(Vector3 directon, bool isPlayer)
    {
        if (Projectile != null && CanShoot())
        {
            this.transform.LookAt(this.transform.position + directon);
            _counter = Cooldown;
            BoomProjectileScript projectile = Instantiate(Projectile, Barrel.transform.position, this.transform.rotation);
            projectile.IsPlayerProjectile = isPlayer;
            projectile.Damage = BoomDamage;
            projectile.transform.rotation = Quaternion.Euler(90, projectile.transform.rotation.eulerAngles.y, 0);
            projectile.Shoot(directon, ProjectileForce);
        }
    }


    private void Update()
    {
        if (_counter > 0)
        {
            _counter -= Time.deltaTime;
        }
        else
        {

        }
    }

    internal bool CanShoot()
    {
        return _counter <= 0 && NoObstacle();
    }

    public bool NoObstacle()
    {

        LayerMask mask = LayerMask.GetMask("Default");


        if (Physics.Linecast(transform.position, Barrel.transform.position, mask))
        {
            return false;
        }

        return true;
    }
}
