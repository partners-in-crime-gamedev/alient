using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.Collections;
using UnityEngine;
using UnityEngine.Rendering;

public class PlayerCamera : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> Players;

    [SerializeField] private Volume _damagedVolume;
    [SerializeField] private AnimationCurve _damageStrengthCurve;
    [SerializeField] private Volume _heartbeatVolume;
    [SerializeField] private AnimationCurve _heartbeatCurve;
    private float _heartbeatTimer;

    [ReadOnly, SerializeField]
    public Vector3 MiddlePoint;

    public Vector2 MovementBorder;

    [SerializeField]
    [Range(0.01f, 1f)]
    private float smoothSpeed = 0.5f;

    private Vector3 velocity = Vector3.zero;

    public FMODUnity.EventReference fmodEvent;
    private FMOD.Studio.EventInstance instance;

    [SerializeField]
    private GameObject artefactPlasmaLauncher;
    [SerializeField]
    private GameObject artefactBlink;
    [SerializeField]
    private GameObject artefactShield;

    private bool plasmaGained = false;
    private bool blinkGained = false;
    private bool shieldGained = false;

    private PlayerCharacter playerCharacter;

    private void Start()
    {
        // Calculate the planes from the main camera's view frustum
        //Plane[] planes = GeometryUtility.CalculateFrustumPlanes(Camera.main);

        // Create a "Plane" GameObject aligned to each of the calculated planes
        //for (int i = 0; i < 6; ++i)
        //{
        //	GameObject p = GameObject.CreatePrimitive(PrimitiveType.Plane);
        //	p.name = "Plane " + i.ToString();
        //	p.transform.position = -planes[i].normal * planes[i].distance;
        //	p.transform.rotation = Quaternion.FromToRotation(Vector3.up, planes[i].normal);
        //}

        instance = FMODUnity.RuntimeManager.CreateInstance(fmodEvent);
		//instance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(MiddlePoint));
        instance.start();

        playerCharacter = FindObjectOfType<PlayerCharacter>();

    }

    void Update()
    {
        if (playerCharacter == null)
            playerCharacter = FindObjectOfType<PlayerCharacter>();

        if (playerCharacter == null)
            return;

        UpdateMiddlePoint();

        transform.position = Vector3.SmoothDamp(transform.position, MiddlePoint, ref velocity, smoothSpeed);
        
        instance.setParameterByName("PlasmaBall", Vector3.Distance(MiddlePoint, artefactPlasmaLauncher.transform.position));
        instance.setParameterByName("Blink", Vector3.Distance(MiddlePoint, artefactBlink.transform.position));
        instance.setParameterByName("Shield", Vector3.Distance(MiddlePoint, artefactShield.transform.position));

        AbilityTracks();

        if (_damagedVolume && Players.Count > 0)
        {
            var weight = _damageStrengthCurve.Evaluate(Players.Min(x =>
            {
                var stats = x.GetComponent<EntityStats>();
                if (stats)
                {
                    instance.setParameterByName("Health", stats.CurrentHealth/stats.MaxHealth);
                    return stats.CurrentHealth / stats.MaxHealth;
                }

                return 1f;
            }));

            _damagedVolume.weight = weight;
            _heartbeatVolume.weight = Mathf.Lerp(0, _heartbeatCurve.Evaluate(_heartbeatTimer), weight);

            _heartbeatTimer += Time.deltaTime * (weight * 30f + 50f) / 60f;

            _heartbeatTimer %= 1f;

        }

    }

    public void GainAbility(AbilityType ability) 
    {
            switch (ability) 
            {
            case AbilityType.Plasma:
                plasmaGained = true;
                break;
            case AbilityType.Blink:
                blinkGained = true;
                break;
            case AbilityType.Shield:
                shieldGained = true;
                break;
            default:
                break;
            }
    }

    void AbilityTracks() 
    {
        if (plasmaGained || playerCharacter.HasPlasma) 
            instance.setParameterByName("PlasmaBall", 0);
        
        if (blinkGained || playerCharacter.HasBlink) 
            instance.setParameterByName("Blink", 0);
        
        if (shieldGained || playerCharacter.HasShield)
            instance.setParameterByName("Shield", 0);
    }

    public void StopMusic() 
    {
        instance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
    }

    private void UpdateMiddlePoint()
    {
        if (Players.Count > 0)
        {
            Vector3 vec = Vector3.zero;
            for (int i = 0; i < Players.Count; i++)
            {
                vec += Players[i].transform.position;
            }
            MiddlePoint = vec / Players.Count;
        }
        else
        {
            //MiddlePoint = Vector3.zero;
        }
    }

    public void AddPlayer(GameObject newPlayer)
    {
        if(!Players.Contains(newPlayer))
            Players.Add(newPlayer);
    }
    public void RemovePlayer(GameObject player)
    {
        if(Players.Contains(player))
            Players.Remove(player);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;

        Vector3 vector3 = new Vector3(MovementBorder.x, 0f, MovementBorder.y);

        Gizmos.DrawLine(MiddlePoint + vector3 + Vector3.up, MiddlePoint - vector3 + Vector3.up);
    }

    internal bool PlayerInBounds(Vector3 position)
    {
        return position.x < transform.position.x + MovementBorder.x &&
            position.z < transform.position.z + MovementBorder.y &&
            position.x > transform.position.x - MovementBorder.x &&
            position.z > transform.position.z - MovementBorder.y;
    }

    internal Vector3 ClampPlayerPosition(Vector3 position)
    {
        Vector3 newPos = position;

        if (position.x > transform.position.x + MovementBorder.x)
        {
            newPos.x = transform.position.x + MovementBorder.x;
        }
        else if (position.x < transform.position.x - MovementBorder.x)
        {
            newPos.x = transform.position.x - MovementBorder.x;
        }
        if (position.z > transform.position.z + MovementBorder.y)
        {
            newPos.z = transform.position.z + MovementBorder.y;
        }
        else if (position.z < transform.position.z - MovementBorder.y)
        {
            newPos.z = transform.position.z - MovementBorder.y;
        }

        return newPos;
    }

    internal void ResetPosition()
    {
        UpdateMiddlePoint();
        transform.position = MiddlePoint;
    }
}
