using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(Rigidbody))]
public class BoomProjectileScript : MonoBehaviour
{
	private Rigidbody rb;


	public float MaxDistance;
	public bool IsPlayerProjectile;

	public float Damage;

	public BoomScript BoomPrefab;

	private Vector3 startingPoint;

	private FMOD.Studio.EventInstance sfx;
	
	// Start is called before the first frame update
	void Start()
	{
		startingPoint = transform.position;
		rb = GetComponent<Rigidbody>();
		
		PlaySFX();
	}

	// Update is called once per frame
	void Update()
	{
		if(Vector3.Distance(startingPoint,transform.position) >= MaxDistance)
			MakeBoom();
	}

	private void PlaySFX() 
	{
		sfx = FMODUnity.RuntimeManager.CreateInstance("event:/SFX/plasmashot");	
		sfx.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject.transform));
		sfx.start();
		sfx.release();
	}

	private void OnTriggerEnter(Collider other)
	{
		if (!other.isTrigger)
		{
			Debug.Log($"PlasmaBall colision triggered by {other.gameObject}");
			MakeBoom();
		}
	}

	public void Shoot(Vector3 Direction, float Speed, float lifespan = 10f)
	{
		if (rb == null)
			rb = GetComponent<Rigidbody>();
		rb.AddForce(Direction.normalized * Speed);
		Destroy(this.gameObject, lifespan);
	}

	public void MakeBoom()
	{
		BoomScript bs = Instantiate(BoomPrefab, this.transform.position, this.transform.rotation);

		bs.Damage = Damage;
		bs.IsPlayerBoom = IsPlayerProjectile;

		this.GetComponentInChildren<TrailRenderer>().transform.SetParent(null);

		Destroy(this.gameObject);
	}
}
