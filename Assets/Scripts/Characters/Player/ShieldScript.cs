using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldScript : MonoBehaviour
{
    public GameObject ShieldGameObject;
    public EntityStats PlayerStats;
    public OnHitScript ShieldOnHit;

    public bool Active;
    public float StartModifier;
    public float EndModifier;
    public float ShieldDuration;

    public AnimationCurve TweenEase = AnimationCurve.Linear(0,0,1,1);

    public float Cooldown = 10;
    private float _counter;

    public float CooldownProgress => 1f - _counter / Cooldown;


    [SerializeField, ReadOnly]
    private float modifier;

    private Sequence currentSeq;

    private FMOD.Studio.EventInstance sfx;

    private MeshRenderer renderer;
    public Vector2 distortionMin;
    public Vector2 distortionMax;
    public float intensityMin;
    public float intensityMax;

    private void Awake()
    {
        if(ShieldOnHit != null)
        {
            ShieldOnHit = ShieldGameObject.GetComponent<OnHitScript>();
            ShieldOnHit.OnHit.AddListener(DelegateDamage);
        }
        if(PlayerStats != null)
            PlayerStats = GetComponentInParent<EntityStats>();

        modifier = 1;


        renderer = GetComponent<MeshRenderer>();

    }

    public void DelegateDamage(object sender, OnHitEventArgs origArgs)
    {
        if(Active)
        {
            OnHitEventArgs args = new OnHitEventArgs()
            {
                PlayerBullet = origArgs.PlayerBullet,
                Damage = origArgs.Damage * modifier,
                Origin = origArgs.Origin,
                Force = origArgs.Force,
            };
            Debug.Log($"Damage mittigated: {origArgs.Damage} => {args.Damage}");
            PlayerStats.GetHit(sender, args);
        }
    }

    private void Start()
    {
        //ActivateShield();
    }

    private void Update()
    {
        if(_counter > 0)
            _counter -= Time.deltaTime;
    }

    public void ActivateShield()
    {
        Active = true;
        PlaySFX();
        ShieldGameObject.SetActive(true);
        modifier = StartModifier;
        Sequence seq = DOTween.Sequence();
        seq.Append(
            DOTween.To(() => { return modifier; }, (val) => modifier = val, EndModifier, ShieldDuration)
            .SetEase(TweenEase));

        seq.OnComplete(DeactivateShield);

        currentSeq = seq;
    }

    public void DeactivateShield()
    {
        Active = false;
        ShieldGameObject.SetActive(false);
        _counter = Cooldown;

        KillTween();
    }

    private void KillTween()
    {
        if (currentSeq != null)
        {
            currentSeq.Kill();
        }
        
        currentSeq = null;
    }

    public bool CanActivate()
    {
        //Debug.Log($"Shield.CanActivate: Active={Active}, _counter={_counter}");
        return !Active && _counter <= 0;
    }

    void PlaySFX()
    {
        sfx = FMODUnity.RuntimeManager.CreateInstance("event:/SFX/shield");
        sfx.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject.transform));
        sfx.start();
        sfx.release();
    }
}
