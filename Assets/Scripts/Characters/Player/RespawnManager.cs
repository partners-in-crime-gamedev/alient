using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RespawnManager : MonoBehaviour
{
    [SerializeField]
    private int currentSpawn = 0;

    [SerializeField]
    private List<PlayerSpawn> spawnList;

	public PlayerSpawn ActiveRespawnPoint => spawnList[currentSpawn];
    

    public void ChangeCurrentSpawnIndex(int index)
    {
        if(index < spawnList.Count)
        {
            if (index > currentSpawn)
            {
                foreach(var entity in FindObjectsOfType<PlayerCharacter>().Select(x=>x.GetComponent<EntityStats>()))
                {
                    entity.ResetHealth();
                }
            }
            currentSpawn = index;
        }
    }

    public void RespawnDeadPlayers(List<PlayerCharacter> players)
    {
        if (players.Any(p=>!p.IsDead))
        {
            var player = players.First(p => !p.IsDead);
            foreach (var playerCharacter in players)
            {
                if (playerCharacter.IsDead)
                {
                    playerCharacter.Respawn(player.transform.position);
                }
            }
        }
        else
        {
            Debug.LogError("Cannot respawn dead, no player is alive!");
        }
    }
}
