using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using Speedrun;
using Unity.Collections;
using UnityEngine;
using UnityEngine.InputSystem;
public class OnPlayerDiedArgs
{
	public PlayerCharacter character;
}

public enum BurstFireMode
{
	Burst,
	Magazine,
	Uninterupted,
	FullAuto
}


[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(EntityStats))]
public class PlayerCharacter : MonoBehaviour
{
	[SerializeField]
	private float Speed;
	[SerializeField]
	private float DashDistance;
	[SerializeField]
	private float DashDuration;
	[SerializeField]
	private float DashCooldown;
	[SerializeField]
	private float DashSpeedModifier;
	[SerializeField]
	private float DashSpeedModifierDuration;

	public float GravityStrength;

	[SerializeField] private Gun gun;
	[SerializeField] private Transform gunParent;
	[SerializeField] private PlasmaLauncher plasmaLauncher;
	[SerializeField] private BlinkScript blink;	
	[SerializeField] private ShieldScript shield;
    public Gun Gun => gun;
    public PlasmaLauncher PlasmaLauncher => plasmaLauncher;
	public BlinkScript Blink => blink;
	public ShieldScript Shield => shield;

	private EntityStats stats;


	private Vector3 _moveDirection;
	private Vector3 _aimDirection;
	private Vector3 _aimPosition;
	private Vector3 _dashDirection;
	private CharacterController _characterController;

	private bool _isDashing = false;
	private float _dashCounter = 0f;
	private float _dashSpeedModCounter = 0f;
	private float _dashTraveled = 0f;
	private float _dashSpeed;

	private PlayerCamera _camera;


	public Animator _animator;
	private bool _isRunning = false;

	[Header("Respawning")]
	public float RespawnTime;
	//[SerializeField]
	//public Transform RespawnPoint;
	private float _respawnCounter;
	private bool _isRespawning;
	private bool _isDead;
	public bool IsDead => _isDead;



	public event EventHandler<OnPlayerDiedArgs> OnPlayerDied;

	private Boolean firstRespawn = true;
	private FMOD.Studio.EventInstance sfx;

	[Header("Abilities")]

	public bool HasPlasma;
	public bool HasBlink;
	public bool HasShield;

	[SerializeField] public GameObject PlasmaProp;
	[SerializeField] public GameObject BlinkProp;
	[SerializeField] public GameObject ShieldProp;

	[Header("Other")]
	private int _shotCount = 0;
	public int BurstLength;
	public BurstFireMode BurstMode = BurstFireMode.Burst;
	public float BurstCooldown = 0;
	private float _burstCounter = 0f;

	private bool _isShooting = false;
	private bool _isBlinking = false;
    [SerializeField] private LayerMask _layerMask;
    [SerializeField] private GunLaser _gunLaser;


    private void Start()
	{
		if (_characterController == null)
			_characterController = GetComponent<CharacterController>();
		_dashSpeed = DashDistance / DashDuration;

		_camera = FindObjectOfType<PlayerCamera>();

		if (stats == null)
			stats = GetComponent<EntityStats>();

		stats.OnDeath.AddListener(OnDeath);

		_aimDirection = Vector3.forward;

		firstRespawn = true;

	}

    private void OnValidate()
	{
		_dashSpeed = DashDistance / DashDuration;
	}

    public void Init()
	{
		_camera = FindObjectOfType<PlayerCamera>();
	}

	void Update()
	{
        if (CinematicsManager.IsPlaying())
        {
			_animator.gameObject.SetActive(false);
        }
        else
        {
            _animator.gameObject.SetActive(true);
        }
		if (!_camera) return;
		if (_isDead)
		{
			//_respawnCounter -= Time.deltaTime;

			//if(_respawnCounter <= 0)
			//         {
			//	Respawn();
			//         }


			return;
		}

		if (_isRespawning)
		{
			_isRespawning = false;
			FindObjectOfType<PlayerCamera>().ResetPosition();
			//if(HasShield)
			//	shield.ActivateShield();
			return;
		}

		if (_isDashing)
		{
			_animator.gameObject.transform.rotation = Quaternion.LookRotation(_moveDirection);

			Vector3 move = _dashDirection * _dashSpeed * Time.deltaTime;
			_characterController.Move(move);
			_dashTraveled += move.magnitude;
			if (_dashTraveled > DashDistance)
			{
				if (_animator != null && _isDashing == true)
				{
					_animator.SetBool("Dash", false);
				}
				_isDashing = false;
				_dashSpeedModCounter = DashSpeedModifierDuration;
                _gunLaser.gameObject.SetActive(true);

			}
		}
		else
		{
			_animator.gameObject.transform.rotation = Quaternion.LookRotation(_aimDirection);
			gunParent.rotation = Quaternion.LookRotation(_aimDirection);
			Vector3 euler = _animator.gameObject.transform.rotation.eulerAngles;
			_animator.gameObject.transform.rotation = Quaternion.Euler(euler + new Vector3(0, 11, 0));

			float tmpSpeed = Speed;
			if (_dashSpeedModCounter > 0)
			{
				tmpSpeed *= _dashSpeedModCounter;
			}
			if (_moveDirection.magnitude > 0)
			{
				//_animator.gameObject.transform.rotation = Quaternion.LookRotation(_moveDirection);
				_isRunning = true;
			}
			else
			{
				_isRunning = false;
			}

			if (_animator != null)
			{
				//_animator.SetBool("Run", _isRunning);
			}
			_characterController.Move(_moveDirection * tmpSpeed * Time.deltaTime + Vector3.down * GravityStrength * Time.deltaTime);



			if (_isShooting)
			{
				if (_aimDirection.magnitude > 0)
				{
					if (_burstCounter <= 0 && gun.CanShoot())
					{
						gun.Fire(_aimDirection, true);
						_shotCount++;
						if (_shotCount >= BurstLength && BurstMode != BurstFireMode.FullAuto)
						{
							_shotCount = 0;
							_isShooting = false;
							_burstCounter = BurstCooldown;
						}
					}
				}
			}
		}

		if (!_camera.PlayerInBounds(transform.position))
		{
			transform.position = _camera.ClampPlayerPosition(transform.position);
		}

		UpdateCooldowns(Time.deltaTime);

		var localVelocityDirection = _animator.transform.InverseTransformDirection(_characterController.velocity);
		_animator.SetFloat("RunDirectionX", localVelocityDirection.x);
		_animator.SetFloat("RunDirectionY", localVelocityDirection.z);
	}

	private void SetRigidbodiesKinematic(bool isKinematic)
	{
		Rigidbody[] bodies = GetComponentsInChildren<Rigidbody>();

		foreach (Rigidbody b in bodies)
		{
			b.isKinematic = isKinematic;
		}
	}

	public void StopMovement()
    {
		_moveDirection = Vector3.zero;
    }

	private void OnDeath(object sender, OnDeathEventArgs e)
	{
		_isDead = true;
		_respawnCounter = RespawnTime;
		GetComponent<Collider>().enabled = false;
		_characterController.enabled = false;
		_animator.enabled = false;
		SetRigidbodiesKinematic(false);

		FindObjectOfType<PlayerCamera>().RemovePlayer(this.gameObject);
        GetComponentInChildren<GunLaser>()?.gameObject.SetActive(false);

		PlaySFX(_isDead);

		_moveDirection = Vector3.zero;
		shield.DeactivateShield();
		blink.Indicator.SetDisplaying(false);

		OnPlayerDied.Invoke(this, new OnPlayerDiedArgs { character = this });
	}

	public void Respawn(Vector3 spawnPosition)
	{
		_isDead = false;
		_isRespawning = true;
		_animator.enabled = true;
		SetRigidbodiesKinematic(true);

		if (_characterController == null)
			_characterController = GetComponent<CharacterController>();

		FindObjectOfType<PlayerCamera>().AddPlayer(this.gameObject);
        GetComponentInChildren<GunLaser>(true)?.gameObject.SetActive(true);

        _characterController.enabled = false;
		_moveDirection = Vector3.zero;

		string str = $"PlayerLocationChangeDueRespawn {transform.position} -> {spawnPosition} = ";
		transform.position = spawnPosition;
		Debug.Log(str + transform.position);

		_characterController.enabled = true;
		GetComponent<Collider>().enabled = true;

		if (stats == null)
			stats = GetComponent<EntityStats>();
		stats.ResetHealth();

		PlaySFX(_isDead);

		Debug.Log(transform.position);
	}

	public void OnMove(InputAction.CallbackContext context)
	{
		if (PauseManager.IsPaused || CinematicsManager.IsPlaying())
		{
            _moveDirection = new Vector3(0, 0, 0);
            return;
		}
		if (_isDead)
			return;

		Vector2 direction = context.ReadValue<Vector2>();
		_moveDirection = new Vector3(direction.x, 0, direction.y);
	}

	public void OnDash(InputAction.CallbackContext context)
	{
		if (PauseManager.IsPaused || CinematicsManager.IsPlaying())
		{
			return;
		}
		if (!_isDashing && _dashCounter <= 0 && _moveDirection.magnitude != 0 && context.performed)
		{
			if (_animator != null && _isDashing == false)
			{
				_animator.SetBool("Dash", true);
			}
			_isDashing = true;
			_dashCounter = DashCooldown;
			_dashDirection = _moveDirection;
			_dashTraveled = 0f;

            _gunLaser.gameObject.SetActive(false);

			sfx = FMODUnity.RuntimeManager.CreateInstance("event:/SFX/dash");
			sfx.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject.transform));
			sfx.start();
			sfx.release();

		}
	}

	public void OnAim(InputAction.CallbackContext context)
	{
		if (PauseManager.IsPaused || CinematicsManager.IsPlaying())
		{
			return;
		}

		if (context.control.device is Mouse)
		{
			OnDirectAim(context);
		}
		else
		{
			Vector2 direction = context.ReadValue<Vector2>();
            if (direction.magnitude > 0)
                SetAimDirection(new Vector3(direction.x, 0, direction.y));
        }

	}

	public void OnDirectAim(InputAction.CallbackContext context)
	{
		if (PauseManager.IsPaused || CinematicsManager.IsPlaying())
		{
			return;
		}
		Vector2 position = context.ReadValue<Vector2>();
		Ray ray = Camera.main.ScreenPointToRay(position);
		RaycastHit raycastHit;
        if (Physics.Raycast(ray, out raycastHit, Mathf.Infinity, _layerMask))
		{
			_aimPosition = raycastHit.point;
			Vector3 direction = _aimPosition - transform.position;
			SetAimDirection(direction);
		}
	}

	private void SetAimDirection(Vector3 direction)
    {
		//Debug.Log("--------------------------------------------");
		//Debug.Log($"direction={direction}; mag={direction.magnitude}");
		//blink.transform.LookAt(direction);
		_aimDirection = new Vector3(direction.x, 0, direction.z);
		//Debug.Log($"direction={_aimDirection}; mag={_aimDirection.magnitude}");
		_aimDirection.Normalize();
		//Debug.Log($"direction={_aimDirection}; mag={_aimDirection.magnitude}");
		//_aimDirection = new Vector3(_aimDirection.x, 0, _aimDirection.z);
		//Debug.Log($"direction={_aimDirection}; mag={_aimDirection.magnitude}");
		//Debug.Log("--------------------------------------------");
		blink.UpdateAim(_aimDirection);
		gun.Aim(_aimDirection);
	}

	public void OnFire(InputAction.CallbackContext context)
	{
		if (PauseManager.IsPaused)
		{
			return;
		}
		if (CinematicsManager.IsPlaying())
		{
			if (context.performed)
			{
				CinematicsManager.TriggerSkip();
			}
			return;
		}

		if (!context.performed && BurstMode == BurstFireMode.Uninterupted)
			return;

		_isShooting = context.performed;

		if (!context.performed && BurstMode != BurstFireMode.Magazine)
		{
			_shotCount = 0;
			_burstCounter = BurstCooldown;
		}
		//Debug.Log($"Set _isShooting to {_isShooting}");
	}

	public void OnPlasmaBall(InputAction.CallbackContext context)
	{
		if (PauseManager.IsPaused)
		{
			return;
		}
		if (!HasPlasma || IsDead)
			return;

		if (plasmaLauncher.CanShoot() && context.performed && _aimDirection.magnitude > 0)
			plasmaLauncher.Fire(_aimDirection, true);
		//Debug.Log($"Launch Plasma {_isShooting}");
	}

	public void OnTeleport(InputAction.CallbackContext context)
	{
		if (PauseManager.IsPaused || CinematicsManager.IsPlaying())
			return;

		if (!HasBlink || IsDead)
			return;

		if (_aimDirection.magnitude > 0)
        {
			if (context.performed)
			{
				blink.SetUp();
			}
			else if (!context.started)
			{
				blink.Blink();
			}
        }
    }

	public void OnShield(InputAction.CallbackContext context)
	{
		if (PauseManager.IsPaused || CinematicsManager.IsPlaying())
			return;

		if (!HasShield || IsDead)
			return;

		if (shield.CanActivate() && context.performed)
		{
			shield.ActivateShield();
		}
	}

	public void OnPause(InputAction.CallbackContext context)
	{
		if(context.performed)
			PauseManager.PauseGame();
	}

	private void UpdateCooldowns(float deltaTime)
	{
		if(_dashCounter > 0)
			_dashCounter -= deltaTime;
		if(_dashSpeedModCounter > 0)
			_dashSpeedModCounter -= deltaTime;
		if(_burstCounter > 0)
			_burstCounter -= deltaTime;
	}

	public void GainAbility(AbilityType abilityType)
	{
		switch(abilityType)
        {
			case AbilityType.Plasma:
				HasPlasma = true;
				PlasmaProp.SetActive(true);
				break;
			case AbilityType.Shield:
				HasShield = true;
				ShieldProp.SetActive(true);
				break;
			case AbilityType.Blink:
				HasBlink = true;
				BlinkProp.SetActive(true);
				break;
			default:
				break;
        }
	}


	private void PlaySFX(bool death)
	{
		if (death)
		{
			sfx = FMODUnity.RuntimeManager.CreateInstance("event:/SFX/alien-death");
		}
		else
		{	
			if (!firstRespawn)
            {
				sfx = FMODUnity.RuntimeManager.CreateInstance("event:/SFX/resurrection");
			} else
            {
				firstRespawn = false;
            }
		}
		sfx.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject.transform));
		sfx.start();
		sfx.release();

	}


	private void OnDrawGizmos()
	{
		Vector3 gizmoLocation = transform.position + Vector3.up;


		Gizmos.color = Color.yellow;
		if (_dashCounter > 0)
			Gizmos.DrawSphere(gizmoLocation, (_dashCounter / DashCooldown) * 0.5f);

		Gizmos.color = Color.red;
		if (_dashSpeedModCounter > 0)
			Gizmos.DrawSphere(gizmoLocation + Vector3.right * 0.5f, (_dashSpeedModCounter / DashSpeedModifierDuration) * 0.5f);

		Gizmos.color = Color.red;
		Gizmos.DrawLine(gizmoLocation, gizmoLocation + (_aimDirection * 2f));

		//Gizmos.color = Color.green;
		//if (_aimPosition != null)
		//	Gizmos.DrawSphere(_aimPosition, 1f);
	}
}
