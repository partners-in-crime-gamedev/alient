	using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoomScript : MonoBehaviour
{
	public float StartScale = 0f;
	public float EndScale = 4f;

	public float BoomDuration = 1f;
	public float BoomFadeout = 0.1f;

	public bool IsPlayerBoom;

	public float Damage;
	public float ExplosionForce;
	public bool FireExtinguisherExplosion = false;

    private FMOD.Studio.EventInstance sfx;

	// Start is called before the first frame update
	void Start()
	{
		PlaySFX(FireExtinguisherExplosion);

		this.transform.localScale = Vector3.one * StartScale;

		Material material = this.GetComponent<MeshRenderer>().material;

        //material.SetColor("Edge Color", Color.black);
        //this.GetComponent<MeshRenderer>().material.SetColor("Edge Color", Color.black);

        Sequence seq = DOTween.Sequence();
		seq.Append(this.transform.DOScale(Vector3.one * EndScale, BoomDuration));
		seq.Append(this.transform.DOScale(Vector3.zero, BoomFadeout).SetEase(Ease.InQuad));
		//seq.Append(DOTween.To(() => material.GetColor("Color"), (Color x) => material.SetColor("Color", x), Color.black, BoomFadeout));

		//seq.Join(DOTween.To(() => material.GetColor("Edge Color"), (Color x) => material.SetColor("Edge Color", x), Color.black, BoomFadeout));
		//seq.Join(DOTween.To(() => material.GetColor("TiltColor"), (Color x) => material.SetColor("TiltColor", x), Color.black, BoomFadeout));
		//seq.Join(DOTween.To(() => material.GetFloat("DisplacementStrength"), (float x) => material.SetFloat("DisplacementStrength", x), 0f, BoomFadeout));
		//seq.Join(DOTween.To(() => material.GetFloatArray("Strength")[0], (float x) => material.SetFloatArray("Strength", new float[] { x, x }), 0f , BoomFadeout));
		//seq.Append(material.DOFloat(0f, "DisplacementStrength", BoomFadeout));
		//seq.Join(material.DOColor(Color.black, "Color", BoomFadeout));
		//seq.Join(material.DOColor(Color.black, "Edge Color", BoomFadeout));
		//seq.Join(material.DOColor(Color.black, "TiltColor", BoomFadeout));
		//seq.Join(material.DOVector(Vector4.zero, "Strength", BoomFadeout));
		seq.onComplete = KillMe;
    }

    private void KillMe()
    {
        Destroy(this.gameObject);
    }

	private void PlaySFX(bool fire_extinguisher) 
	{
		string path = "event:/SFX/plasmaball";
		if (fire_extinguisher) { path = "event:/SFX/fire-extinguisher-bubbles"; }

		sfx = FMODUnity.RuntimeManager.CreateInstance(path);	
		sfx.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject.transform));
		sfx.start();
		sfx.release();
	}


    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("------------------------------------------------------");

        //GameObject go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        //go.transform.position = other.transform.position;

        OnHitScript onHitScript = other.GetComponent<OnHitScript>();
        
		if (onHitScript != null)
		{
			var origin = transform.position;
			origin.y = 0;
			var args = new OnHitEventArgs()
			{
				PlayerBullet = this.IsPlayerBoom,
				Damage = this.Damage,
				Origin = origin,
				Force = this.ExplosionForce,
			};

			onHitScript.InvokeOnHit(args);
		}
    }
}
