using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using UnityFx.Outline;

public class OnNewPlayerAddedArgs
{
    public GameObject character;
}

[RequireComponent(typeof(PlayerInputManager))]
public class PlayerManager : MonoBehaviour
{
    List<PlayerCharacter> Players = new List<PlayerCharacter>();

    [SerializeField]
    private Material georgeMaterial;
    [SerializeField]
    private Material eugeneMaterial;
    [SerializeField] private PlayerSpawn spawnPoints;
    private PlayerInputManager inputManager;
    [SerializeField] private OutlineSettings outlineSettings1;
    [SerializeField] private OutlineSettings outlineSettings2;
    [SerializeField] private UIPlayerHUD playerHud1;
    [SerializeField] private UIPlayerHUD playerHud2;
    [SerializeField] private CanvasGroup gameOverTextGroup;

    public UnityEvent<OnNewPlayerAddedArgs> OnPlayerAdded;

    public float RespawnDelay = 2;
    [SerializeField] private Renderer introAlienRenderer;

    public int PlayerCount => Players.Count;
    public int AliveCount => Players.Count(x=>!x.IsDead);

    public static UnityEvent OnGameOver = new UnityEvent();


    // Start is called before the first frame update
    void Awake()
    {
        inputManager = GetComponent<PlayerInputManager>();
        if (MainMenuManager.IsSinglePlayerGame)
        {
            inputManager.DisableJoining();
        }
        else
        {
            inputManager.EnableJoining();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OnPlayerJoined(PlayerInput playerInput)
    {
        Debug.Log("new player");
        var playerCharacter = playerInput.gameObject.GetComponent<PlayerCharacter>();
        Players.Add(playerCharacter);
        playerCharacter.Init();
        var controller = Players[playerInput.playerIndex].GetComponent<CharacterController>();
        controller.enabled = false;
        //if (spawnPoints.Length >= playerInput.playerIndex + 1)
        //{
        //    playerInput.transform.position = spawnPoints[playerInput.playerIndex].position;
        //}

        spawnPoints.SpawnPlayer(playerCharacter,playerInput.playerIndex);

        controller.enabled = true;

        Debug.Log($"Players.Count={Players.Count}");

        if (Players.Count == 1)
        {
            SetGeorge(playerInput.gameObject);
            var outlineData = playerInput.GetComponent<PlayerOutlineData>();
            outlineData.outlineRenderer.gameObject.layer = LayerMask.NameToLayer(outlineSettings1.name);
            outlineSettings1.OutlineColor = outlineData.playerRenderer.material.color;
            if (introAlienRenderer)
            {
                introAlienRenderer.material.color = outlineSettings1.OutlineColor;
            }
            if(playerHud1)
                playerHud1.SetupPlayer(playerInput.gameObject);
        }
        else if (Players.Count == 2)
        {
            SetEugene(playerInput.gameObject);
            var outlineData = playerInput.GetComponent<PlayerOutlineData>();
            outlineData.outlineRenderer.gameObject.layer = LayerMask.NameToLayer(outlineSettings2.name);
            outlineSettings2.OutlineColor = outlineData.playerRenderer.material.color;
            if(playerHud2)
                playerHud2.SetupPlayer(playerInput.gameObject);
        }
        else
		{
            SetGeneric(playerInput.gameObject);
		}

        playerCharacter.OnPlayerDied += PlayerDied;

        FindObjectOfType<PlayerCamera>().AddPlayer(playerInput.gameObject);

        Debug.Log("Invoke OnPlayerAdded");
        OnPlayerAdded.Invoke(new OnNewPlayerAddedArgs { character = playerInput.gameObject });
    }

    private void PlayerDied(object sender, OnPlayerDiedArgs e)
    {
        bool allDead = true;
        foreach(PlayerCharacter item in Players)
        {
            if(!item.IsDead)
                allDead = false;

            HealthPickup[] healthPickups = FindObjectsOfType<HealthPickup>();

            foreach(HealthPickup healthPickup in healthPickups)
            {
                Destroy(healthPickup.gameObject);
            }
        }

        if (allDead)
        {
            Invoke(nameof(GameOver),RespawnDelay);
            if (gameOverTextGroup)
            {
                gameOverTextGroup.gameObject.SetActive(false);
                gameOverTextGroup.gameObject.SetActive(true);
                gameOverTextGroup.DOFade(1, RespawnDelay-0.3f);
            }
        }
    }

    private void GameOver()
    {
        Debug.Log("GAME OVER");
        StatsManager.AddDeath();

        FindObjectOfType<RespawnManager>().ActiveRespawnPoint.RespawnPlayers(Players);
        gameOverTextGroup.gameObject.SetActive(false);
        gameOverTextGroup.gameObject.SetActive(true);
        gameOverTextGroup.DOFade(0, 0.1f);

        OnGameOver.Invoke();
    }

    public void RespawnDeadPlayers()
    {
        FindObjectOfType<RespawnManager>().RespawnDeadPlayers(Players);
    }

    private void SetGeorge(GameObject player)
	{
        Debug.Log($"SetGeorge: Players.Count={Players.Count}");
        player.GetComponent<MeshRenderer>().material = georgeMaterial;
        player.name = "George";
    }

    private void SetEugene(GameObject player)
    {
        Debug.Log($"SetEugene: Players.Count={Players.Count}");
        player.GetComponent<MeshRenderer>().material = eugeneMaterial;
        player.name = "Eugene";
        player.transform.position += Vector3.right;
    }

    private void SetGeneric(GameObject player)
    {
        Debug.Log($"SetGeneric: Players.Count={Players.Count}");
        player.GetComponent<MeshRenderer>().material.color = new Color(UnityEngine.Random.Range(0,1f), UnityEngine.Random.Range(0, 1f), UnityEngine.Random.Range(0, 1f));
        player.name = "Alien Soldier "+Players.Count;
        player.transform.position += Vector3.right * Players.Count;
    }
}
