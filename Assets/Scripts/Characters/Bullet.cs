using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(Rigidbody))]
public class Bullet : MonoBehaviour
{
	private Rigidbody rb;
	public bool IsPlayerBullet;

	public float Damage;
	public float Force = 1000f;

    [SerializeField] private Transform trailTransform;
    [SerializeField] private GameObject hitEnvironmentParticles;
    [SerializeField] private GameObject hitLivingParticles;
    [SerializeField] private LayerMask livingMask;

    private void Start()
	{
		rb = GetComponent<Rigidbody>();
	}

	public void Shoot(Vector3 Direction, float Speed, float lifespan = 10f)
	{
		if(rb == null)
			rb = GetComponent<Rigidbody>();
		rb.AddForce(Direction.normalized * Speed);
		Destroy(gameObject,lifespan);
	}

    private void OnCollisionEnter(Collision col)
    {
        if (hitLivingParticles && hitEnvironmentParticles)
        {
            if ((livingMask & (1 << col.gameObject.layer)) != 0)
            {
                Instantiate(hitLivingParticles, col.GetContact(0).point, Quaternion.LookRotation(col.GetContact(0).normal));
            }
            else
            {
                Instantiate(hitEnvironmentParticles, col.GetContact(0).point, Quaternion.LookRotation(col.GetContact(0).normal));
            }
		}

        OnHitScript onHitScript = col.gameObject.GetComponent<OnHitScript>();

        //Debug.Log($"Hit: other:{other}, onHitScript:{onHitScript}");

        if (onHitScript != null)
        {

            var args = new OnHitEventArgs()
            {
                PlayerBullet = this.IsPlayerBullet,
                Damage = this.Damage,
                Origin = transform.position - rb.velocity.normalized * 0.5f,
                Force = this.Force
            };

            onHitScript.InvokeOnHit(args);
        }

        if (trailTransform)
        {
            trailTransform.SetParent(null);
        }


        Destroy(gameObject);
    }
}
