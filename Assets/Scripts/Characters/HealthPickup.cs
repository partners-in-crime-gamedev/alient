using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class HealthPickup : MonoBehaviour
{
    public Transform Visualisation;
    public float Health = 20f;

    public float AnimationTime = 0.3f;
    private FMOD.Studio.EventInstance sfx;

    private Sequence sequence;
    

    void Awake()
    {
        //Sequence seq = DOTween.Sequence();
        //seq.Append(Visualisation.transform.DORotate(new Vector3(0, 90, 0), 2.5f)
        //    .SetLoops(4, LoopType.Incremental)
        //    .SetEase(Ease.Linear));
        //seq.Join(Visualisation.transform.DOMoveY(Visualisation.transform.position.y + 0.2f, 1f)
        //    .SetLoops(10, LoopType.Yoyo)
        //    .SetEase(Ease.InOutQuad));

        //seq.SetLoops(-1, LoopType.Incremental);

        //sequence = seq;

        Visualisation.transform.DORotate(new Vector3(0, 90, 0), 2.5f)
            .SetLoops(-1, LoopType.Incremental)
            .SetEase(Ease.Linear);
        Visualisation.transform.DOMoveY(Visualisation.transform.position.y + 0.2f, 1f)
            .SetLoops(10, LoopType.Yoyo)
            .SetEase(Ease.InOutQuad);
    }

    private void OnTriggerStay(Collider other)
    {
        EntityStats entitystats = other.GetComponent<EntityStats>();

        if(sequence == null && entitystats != null && entitystats.IsPlayer && !entitystats.IsDead() && entitystats.IsHurt())
        {
            //sequence.Kill();
            PlaySFX();

            //entitystats.GetHealed(Health);
            //Destroy(this.gameObject);
            
            sequence = DOTween.Sequence();

            sequence.Append(this.transform.DOMove(entitystats.transform.position, AnimationTime).SetEase(Ease.InQuad));

            sequence.OnComplete(() => 
            {
                if (!entitystats.IsDead())
                {
                    entitystats.GetHealed(Health);
                    Kill();
                }
                else
                {
                    sequence.Kill();
                    sequence = null;
                }
            });
        }
    }

    void PlaySFX()
    {
        sfx = FMODUnity.RuntimeManager.CreateInstance("event:/SFX/health-pickup");
        sfx.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject.transform));
        sfx.start();
        sfx.release();
    }

    public void Kill()
    {
        sequence.Kill();

        Destroy(gameObject);
    }
}
