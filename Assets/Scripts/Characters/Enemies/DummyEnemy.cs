using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(EntityStats))]
[RequireComponent(typeof(OnHitScript))]
public class DummyEnemy : MonoBehaviour
{
	private EntityStats stats;
	//private OnHitScript onHitScript;

	void Start()
	{
		stats = GetComponent<EntityStats>();
		//onHitScript = GetComponent<OnHitScript>();

		//onHitScript.OnHit += stats.GetHit;
		stats.OnDeath.AddListener(Die);
	}

    private void Die(object sender, OnDeathEventArgs e)
    {
		//onHitScript.OnHit -= stats.GetHit;
		GetComponent<OnHitScript>().OnHit.RemoveListener(stats.GetHit);
		transform.Rotate(90, 0, 0);
		Destroy(this.GetComponent<CapsuleCollider>());
    }
}
