using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using Speedrun;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.InputSystem;

[RequireComponent(typeof(NavMeshAgent))]
public class BraindeadAI : MonoBehaviour
{
    [SerializeField]
    public List<PlayerCharacter> TargetList;
    private bool _targettingBlinkParticles;
    private GameObject _target;

    public GameObject Target
    {
        get { return _target; }
        set
        {
            if (_target != value)
            {
                if (_target != null)
                {
                    var pc = _target.GetComponent<PlayerCharacter>();
                    if (pc && pc.Blink)
                    {
                        pc.Blink.OnBlinkEvent.RemoveListener(TargetBlinkParticles);
                    }
                }

                if (value != null)
                {
                    var pc = value.GetComponent<PlayerCharacter>();
                    if (pc && pc.Blink)
                    {
                        pc.Blink.OnBlinkEvent.AddListener(TargetBlinkParticles);
                    }

                    _target = value;
                }
            }
        }
    }
    public bool TargetHostile = true;

    [SerializeField]
    private NavMeshAgent agent;

    [SerializeField]
    private Vector3 destination;

    public float SafeDistance;

    public bool IsDead = false;

    EnemyAnimator animator;

    public Gun EnemyGun;
    
    private FMOD.Studio.EventInstance sfx;
    private float _targettingBlinkParticlesTimer;

    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<EnemyAnimator>();
        GetComponent<EntityStats>().OnDeath.AddListener(KillMe);
        GetComponent<EntityStats>().OnDamage.AddListener(DamageMe);

        TargetList = new List<PlayerCharacter>(FindObjectsOfType<PlayerCharacter>());

        var manager = FindObjectOfType<PlayerInputManager>();
        manager.playerJoinedEvent.AddListener((input) =>
        {
            TargetList.Add(input.GetComponent<PlayerCharacter>());
        });
        manager.playerLeftEvent.AddListener((input) =>
        {
            TargetList.Remove(input.GetComponent<PlayerCharacter>());
        });
    }

    private void TargetBlinkParticles(GameObject particles, float duration)
    {
        Target = particles;
        if (IsInShootingDistance())
        {
            _targettingBlinkParticles = true;
            _targettingBlinkParticlesTimer = duration;
        }
    }

    private void DamageMe(object arg0, OnHitEventArgs arg1)
    {
        SetHostile(true);
        if (_targettingBlinkParticles)
        {
            _targettingBlinkParticles = false;
            Target = PickRandomTarget();
            agent.SetDestination(Target.transform.position);
        }
    }

    private void KillMe(object sender, OnDeathEventArgs e)
    {
        IsDead = true;
        agent.isStopped = true;
        agent.enabled = false;
        Destroy(GetComponent<CapsuleCollider>());
        animator.Animator.enabled = false;

        transform.SetParent(null);

        GetComponentInChildren<GunLaser>()?.gameObject.SetActive(false);

        //FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/enem-kill");
        sfx = FMODUnity.RuntimeManager.CreateInstance("event:/SFX/enemy-death");
        sfx.start();
        sfx.release();
        sfx.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject.transform));
        //transform.DOMove(transform.position + Vector3.down * 3, 1f);
        //Destroy(this.gameObject,1f);

        StatsManager.AddKill();
    }

    private void Update()
    {
        if (IsDead || PauseManager.IsPaused || CinematicsManager.IsPlaying())
            return;

        if (_targettingBlinkParticles)
        {
            if (_targettingBlinkParticlesTimer > 0f)
            {
                _targettingBlinkParticlesTimer -= Time.deltaTime;
            }
            else
            {
                _targettingBlinkParticles = false;
                Target = null;
            }
        }

        if (_targettingBlinkParticles && Target == null)
        {
            _targettingBlinkParticles = false;
        }

        if (Target == null)
        {
            
            Target = PickRandomTarget();
        }

        if (Target == null)
            return;


        if (Target.GetComponent<EntityStats>().IsDead())
        {
            Target = PickRandomTarget();
            return;
        }

        destination = Target.transform.position;

        if ((_targettingBlinkParticles || IsInShootingDistance()) && TargetHostile)
        {
            agent.isStopped = true;
            animator.Stop();
            Shoot();
            return;
        }

        if (EnemyGun == null || (!EnemyGun.CanShoot() || !IsInShootingDistance()) && TargetHostile)
        {
            if (!agent.pathPending || !agent.hasPath)
            {
                agent.isStopped = false;

                agent.destination = destination;

                animator.Run();
            }
            
        }
    }

    private GameObject PickRandomTarget()
    {
        GameObject result = null;

        List<PlayerCharacter> livingPlayers = new List<PlayerCharacter> ();

        foreach (PlayerCharacter player in TargetList)
        {
            if (player != null && !player.IsDead)
            {
                livingPlayers.Add(player);
            }
        }

        if (livingPlayers.Count > 0)
            result = livingPlayers.GetRandom().gameObject;

        return result;
    }

    private void Shoot()
    {
        //TODO this is just temporary solution to enemy not shooting the dead player
        if (Target.GetComponent<EntityStats>().IsDead())
            return;

        //Debug.Log($"Shoot with {EnemyGun}");
        if (EnemyGun == null)
            return;

        if(EnemyGun.CanShoot())
        {
            Vector3 direction = Target.transform.position - this.transform.position;
            this.transform.rotation = Quaternion.LookRotation(direction);
            animator.Shoot(Vector3.forward, EnemyGun.Cooldown);
            EnemyGun.Fire(direction, false);
        }
    }

    private bool IsInShootingDistance()
    {
        Ray ray = new Ray(this.transform.position + Vector3.up, Target.transform.position - this.transform.position);
        RaycastHit hit;

        Debug.DrawLine(ray.origin, Target.transform.position + Vector3.up, Color.white);

        LayerMask mask = LayerMask.GetMask("Enemy","Shield","EnemyProjectile", "Ignore Raycast");

        if (Physics.Raycast(ray, out hit, SafeDistance, ~mask))
        {

            if (hit.collider.GetComponent<PlayerCharacter>() == null)
                return false;

            //draw invisible ray cast/vector
            Debug.DrawLine(ray.origin, hit.point, Color.black);

            float distance = hit.distance;

            //Debug.Log($"distance: {distance}, hit: {hit.collider.gameObject}");

            return distance < SafeDistance;
        }


        return false;
    }

    public void SetHostile(bool hostile)
    {
        TargetHostile = hostile;
    }
}
