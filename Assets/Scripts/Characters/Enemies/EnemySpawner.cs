using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public struct SpawnSetup
{
    public List<EnemyToSpawn> EnemyList;
    public List<EnemyToSpawn> PlasmaEnemyList;
    public List<EnemyToSpawn> BlinkEnemyList;
    public List<EnemyToSpawn> ShieldEnemyList;
    public int MaxEnemies;

    public SpawnSetup(SpawnSetup setup, bool mergePlasma = false, bool mergeBlink = false, bool mergeShield = false)
    {
        EnemyList = new List<EnemyToSpawn>(setup.EnemyList);
        PlasmaEnemyList = new List<EnemyToSpawn>(setup.PlasmaEnemyList);
        BlinkEnemyList = new List<EnemyToSpawn>(setup.BlinkEnemyList);
        ShieldEnemyList = new List<EnemyToSpawn>(setup.ShieldEnemyList);

        if (mergePlasma)
            EnemyList.AddRange(setup.PlasmaEnemyList);
        if (mergeBlink)
            EnemyList.AddRange(setup.BlinkEnemyList);
        if (mergeShield)
            EnemyList.AddRange(setup.ShieldEnemyList);

        MaxEnemies = setup.MaxEnemies;
    }
}

[Serializable]
public struct EnemyToSpawn
{
    public int Count;
    public GameObject EnemyType;
}

public class EnemySpawner : MonoBehaviour
{
    public UnityEvent OnAllEnemiesDead = new UnityEvent();

    public bool DebugShowSpawnPoints = true;

    [SerializeField, Tooltip("If empty the spawners position is used")]
    protected List<Transform> spawnPoints;

    [SerializeField]
    protected List<SpawnSetup> SpawnLists;
    //List<EnemyToSpawn> SpawnList;
    //private GameObject EnemyType;

    public List<SingleSpawn> singleSpawns;

    private SpawnSetup? _currentSpawnSetup;

    public SpawnSetup CurrentSpawnSetup
    {
        get
        {
            if (_currentSpawnSetup == null)
            {
                _currentSpawnSetup = new SpawnSetup(GetSpawnSetup(), GameState.Instance.PlasmaTaken, GameState.Instance.BlinkTaken, GameState.Instance.ShieldTaken);
            }
            return _currentSpawnSetup.Value;
        }
    }

    [SerializeField] protected List<GameObject> SpawnedEnemies;

	[SerializeField] protected List<LockableDoors> _spawnDoors;

	public float SpawnDelay;

	private float _spawnCounter;

    public bool SpawnEnemies;

    [Header("Player Avoidance")]

    public bool AvoidPlayers;

    [Tooltip("Random spawn point that is further then this value from all players. If no such spawnpoint is present, furthest one is picked (based on closest player distance).")]
    public float MinDistance;

    [SerializeField]
    public List <Transform> Players;

    private int _playerCount;


    private void Start()
    {
        SpawnedEnemies = new List<GameObject>();

        FindObjectOfType<PlayerManager>().OnPlayerAdded.AddListener(AddNewPlayer);



        _playerCount = FindObjectOfType<PlayerManager>().PlayerCount;

        
        foreach(SingleSpawn spawn in GetSingleSpawns())
        {
            var spawnedEnemy = spawn.ReSpawn();
            SpawnedEnemies.Add(spawnedEnemy);
            spawnedEnemy.GetComponentInChildren<EntityStats>().OnDeath.AddListener(RemoveKilledEnemy);
        }
    }

    private void Update()
    {
        //if (EnemyType == null)
        //    return;

        if(SpawnEnemies)
        {
            _spawnCounter -= Time.deltaTime;

            for (int i = SpawnedEnemies.Count - 1; i >= 0; i--)
            {
                if (!SpawnedEnemies[i])
                {
                    SpawnedEnemies.RemoveAt(i);
                }
            }

            if (_spawnCounter <= 0 && SpawnedEnemies.Count < CurrentSpawnSetup.MaxEnemies)
            {
                SpawnEnemy();
            }
        }
    }

    private void SpawnEnemy()
    {
        _spawnCounter = SpawnDelay;

        GameObject enemyPrefab = ChooseEnemyToSpawn();

        if (enemyPrefab == null)
        {
	        if (SpawnedEnemies.Count == 0)
	        {
		        foreach (var spawnDoor in _spawnDoors)
		        {
			        spawnDoor.TurnOffDoors();
		        }

                OnAllEnemiesDead.Invoke();
		        Debug.Log($"<color=green>Room {name} cleared!</color>");
                gameObject.SetActive(false);
            }
            
	        return;
        }

        Vector3 spawnPos = GetSpawnPosition();

        GameObject enemy = Instantiate(enemyPrefab, spawnPos,transform.rotation);
        SpawnedEnemies.Add(enemy);
        Debug.Log($"Enemy spawned on {enemy.transform.position}");
        enemy.GetComponent<EntityStats>().OnDeath.AddListener(RemoveKilledEnemy);
        enemy.GetComponent<EntityStats>().OnDeath.AddListener(ActivateSingleSpawnEnemies);
    }

    private void ActivateSingleSpawnEnemies(object arg0, OnDeathEventArgs arg1)
    {
        List<EnemyToSpawn> list = GetSpawnList();

        int sum = list.Sum(x => x.Count);

        if (sum <= 0 && SpawnedEnemies.Count <= singleSpawns.Count)
        {
            foreach (SingleSpawn s in singleSpawns)
            {
                BraindeadAI ai = s.EnemyInstance.GetComponent<BraindeadAI>();

                if (ai != null)
                {
                    ai.SetHostile(true);
                }
                
                TurretAI turret = s.EnemyInstance.GetComponent<TurretAI>();

                if(turret != null)
                {
                    turret.SetHostile(true);
                }
            }
        }
    }

    public void ResetSpawner()
    {
        SpawnEnemies = false;
        foreach (var spawnDoor in _spawnDoors)
        {
            spawnDoor.ResetToInitialState();
        }

        foreach (var spawnedEnemy in SpawnedEnemies)
        {
            Destroy(spawnedEnemy.gameObject);
        }
        SpawnedEnemies.Clear();
        _spawnCounter = 0;
        _currentSpawnSetup = null;
        gameObject.SetActive(true);

        foreach (SingleSpawn spawn in GetSingleSpawns())
        {
            var spawnedEnemy = spawn.ReSpawn();
            SpawnedEnemies.Add(spawnedEnemy);
            spawnedEnemy.GetComponentInChildren<EntityStats>().OnDeath.AddListener(RemoveKilledEnemy);
        }
    }

    private Vector3 GetSpawnPosition()
    {
        if(spawnPoints.Count == 0)
            return transform.position;

        if(!AvoidPlayers)
            return spawnPoints.GetRandom().position;

        float maxMinDist = 0;
        Vector3 maxMinPos = this.transform.position;
        List<Vector3> possiblePos = new List<Vector3>();

        foreach(Transform t in spawnPoints)
        {
            possiblePos.Add(t.position);
            float minDist = float.MaxValue;
            Vector3 minPos = transform.position;
            foreach (Transform p in Players)
            {
                float dist = Vector3.Distance(p.position, t.position);
                if(dist < minDist)
                {
                    minDist = dist;
                    minPos = t.position;
                }
            }
            if(minDist > maxMinDist)
            {
                maxMinDist = minDist;
                maxMinPos = minPos;
            }
        }

        foreach(Transform p in Players)
        {
            for(int i = possiblePos.Count - 1; i >=0; i--)
            {
                if(Vector3.Distance(p.position, possiblePos[i]) < MinDistance)
                {
                    possiblePos.RemoveAt(i);
                }
            }
        }

        if(possiblePos.Count > 0)
            return possiblePos.GetRandom();
        return maxMinPos;
    }

    private List<EnemyToSpawn> GetSpawnList()
    {
        return CurrentSpawnSetup.EnemyList;
    }

    protected virtual SpawnSetup GetSpawnSetup()
    {
        if (_playerCount <= SpawnLists.Count)
            return SpawnLists[_playerCount - 1];
        if (SpawnLists.Count > 0)
            return SpawnLists[0];
        return new SpawnSetup();
    }

    protected virtual List<SingleSpawn> GetSingleSpawns()
    {
        return singleSpawns;
    }

    private GameObject ChooseEnemyToSpawn()
    {
        List<EnemyToSpawn> spawnList = GetSpawnList();

        if (spawnList == null)
            return null;

        int sum = spawnList.Sum(x => x.Count);

        for (int i = 0; i < spawnList.Count; i++)
        {
            EnemyToSpawn enemy = spawnList[i];
            if (UnityEngine.Random.Range(0,sum) < enemy.Count)
            {
                enemy.Count--;
                spawnList[i] = enemy;
                return enemy.EnemyType;
            }
            sum -= enemy.Count;
        }
        return null;
    }

    private void RemoveKilledEnemy(object sender, OnDeathEventArgs e)
    {
        SpawnedEnemies.Remove(e.MurderVictim);
    }

    private void AddNewPlayer(OnNewPlayerAddedArgs e)
    {
        Players.Add(e.character.transform);
        _playerCount++;
    }

    private void OnDrawGizmos()
    {
        if (!DebugShowSpawnPoints)
            return;

        Gizmos.color = Color.blue;

        Gizmos.DrawCube(transform.position, Vector3.one);

        Gizmos.color = Color.cyan;

        foreach(Transform t in spawnPoints)
        {
            if (t != null)
                Gizmos.DrawCube(t.position, Vector3.one);
        }

        //Gizmos.color = Color.red;
        //foreach(SingleSpawn s in singleSpawns)
        //{
        //    if (s != null)
        //        Gizmos.DrawCube(s.transform.position, Vector3.one);
        //}
    }

    public virtual void StartSpawning()
    {
	    if (!SpawnEnemies)
	    {
		    SpawnEnemies = true;
		    foreach (var spawnDoor in _spawnDoors)
		    {
			    spawnDoor.MakeEnemiesOnlyDoors();
		    }
        }
    }
}