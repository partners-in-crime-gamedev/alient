using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EnemySpawnerHub : EnemySpawner
{
	private int _stage = 0;
    [SerializeField] private LockableDoors _elevatorDoorsInside;
    [SerializeField] private LockableDoors _elevatorDoorsOutside;
    [SerializeField] private Transform _elevatorSpawnpoint;

    [Serializable]
	public struct SpawnSetupList
	{
		public List<SpawnSetup> lists;
	}
    
    [Serializable]
	public struct SingleSpawnList
	{
		public List<SingleSpawn> list;
	}


	[SerializeField] private List<SpawnSetupList> _spawnListLists;
	[SerializeField] private List<SingleSpawnList> _SingleSpawnsLists;

	public UnityEvent OnFinalStageReached;// = new UnityEvent();

	protected override SpawnSetup GetSpawnSetup()
	{
		SpawnLists = _spawnListLists[Mathf.Min(_stage, _spawnListLists.Count-1)].lists;
		return base.GetSpawnSetup();
	}

	public void IncreaseStage()
	{
		_stage++;
        if (_stage >= _spawnListLists.Count-1)
        {
			_spawnDoors.Add(_elevatorDoorsInside);
			_spawnDoors.Add(_elevatorDoorsOutside);
			spawnPoints.Add(_elevatorSpawnpoint);
            OnAllEnemiesDead.AddListener(() => { _elevatorDoorsInside.UnlockDoors(); _elevatorDoorsOutside.UnlockDoors(); });
            OnFinalStageReached.Invoke();

		}
		ResetSpawner();
	}

	public void DecreaseStage()
	{
		_stage--;
	}

    protected override List<SingleSpawn> GetSingleSpawns()
    {
		return _SingleSpawnsLists[Mathf.Min(_stage, _SingleSpawnsLists.Count - 1)].list;
    }
}