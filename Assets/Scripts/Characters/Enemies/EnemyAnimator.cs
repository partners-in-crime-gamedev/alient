using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimator : MonoBehaviour
{
	[SerializeField]
	public Animator Animator;

	// Start is called before the first frame update
	void Start()
	{
		Animator = GetComponentInChildren<Animator>();
	}

	// Update is called once per frame
	void Update()
	{
		
	}

    public void Stop()
	{
		Animator.SetBool("Run", false);
		Animator.SetBool("Shoot", false);
	}

    public void Run(Vector3? direction = null)
    {
		if (direction != null)
			this.transform.rotation = Quaternion.LookRotation((Vector3)direction);
		Animator.SetBool("Run", true);
    }

	public void Shoot(Vector3 direction, float delay)
    {
		//this.transform.rotation = Quaternion.LookRotation(direction);
		Animator.SetBool("Shoot", true);

		Invoke("EndShoot", delay);
	}

	private void EndShoot()
    {
		Animator.SetBool("Shoot", false);
    }
}
