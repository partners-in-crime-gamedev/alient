using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Speedrun;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.InputSystem;

[RequireComponent(typeof(EntityStats))]
public class TurretAI : MonoBehaviour
{
    [SerializeField]
    public List<PlayerCharacter> TargetList;

    private bool _targettingBlinkParticles;
    [SerializeField, ReadOnly] private GameObject _target;

    public GameObject Target
    {
        get { return _target; }
        set
        {
            if (_target != value)
            {
                if (_target != null)
                {
                    var pc = _target.GetComponent<PlayerCharacter>();
                    if (pc && pc.Blink)
                    {
                        pc.Blink.OnBlinkEvent.RemoveListener(TargetBlinkParticles);
                    }
                }

                if (value != null)
                {
                    var pc = value.GetComponent<PlayerCharacter>();
                    if (pc && pc.Blink)
                    {
                        pc.Blink.OnBlinkEvent.AddListener(TargetBlinkParticles);
                    }

                    _target = value;
                }
            }
        }
    }

    public bool TargetHostile = true;

    public Vector3 AimingOrigin;
    public float AimDistance;
    [Tooltip("How much must closer target be closer to switch targets.")]
    public float AimSwitchDif; //this is so he doesn't constantly switch targets if they are very close together
    public float ReflexTime;
    public bool IsDead = false;

    EnemyAnimator animator;

    public Gun EnemyGun;

    private FMOD.Studio.EventInstance sfx;

    Vector3 direction;
    private float _targettingBlinkParticlesTimer;

    private void Start()
    {
        animator = GetComponent<EnemyAnimator>();
        animator.Stop();
        GetComponent<EntityStats>().OnDeath.AddListener(KillMe);
        GetComponent<EntityStats>().OnDamage.AddListener(DamageMe);

        TargetList = new List<PlayerCharacter>(FindObjectsOfType<PlayerCharacter>());

        var manager = FindObjectOfType<PlayerInputManager>();
        manager.playerJoinedEvent.AddListener((input) =>
        {
            TargetList.Add(input.GetComponent<PlayerCharacter>());
        });
        manager.playerLeftEvent.AddListener((input) =>
        {
            TargetList.Remove(input.GetComponent<PlayerCharacter>());
        });
    }
    private void TargetBlinkParticles(GameObject particles, float duration)
    {
        Target = particles;
        if (IsInShootingDistance())
        {
            _targettingBlinkParticles = true;
            _targettingBlinkParticlesTimer = duration;
        }
    }

    private void DamageMe(object arg0, OnHitEventArgs arg1)
    {
        SetHostile(true);
        _targettingBlinkParticles = false;
    }

    private void KillMe(object sender, OnDeathEventArgs e)
    {
        IsDead = true;
        Destroy(GetComponent<CapsuleCollider>());
        animator.Animator.enabled = false;
        transform.SetParent(null);

        PlaySFX(false);

        GetComponentInChildren<GunLaser>()?.gameObject.SetActive(false);
        //transform.DOMove(transform.position + Vector3.down * 3, 1f);
        //Destroy(this.gameObject,1f);
        StatsManager.AddKill();
    }

    private void Update()
    {
        if (IsDead || PauseManager.IsPaused || CinematicsManager.IsPlaying())
            return;

        if (_targettingBlinkParticles)
        {
            if (_targettingBlinkParticlesTimer > 0f)
            {
                _targettingBlinkParticlesTimer -= Time.deltaTime;
            }
            else
            {
                _targettingBlinkParticles = false;
            }
        }

        if (_targettingBlinkParticles && Target == null)
        {
            _targettingBlinkParticles = false;
        }

        if (Target == null)
        {

            Target = PickRandomTarget();
        }

        if (Target == null)
            return;


        if (Target.GetComponent<EntityStats>().IsDead())
        {
            Target = PickRandomTarget();
            return;
        }
        else if (!_targettingBlinkParticles)
        {
            Target = PickClosestTarget();
        }

        if (!_targettingBlinkParticles)
        {
            EnemyGun.Aim(direction);
        }

        if ((_targettingBlinkParticles || IsInShootingDistance()) && TargetHostile)
        {
            animator.Stop();
            Shoot();
            return;
        }
    }

    private GameObject PickClosestTarget()
    {
        GameObject newTarget = Target;
        float minDist = Vector3.Distance(Target.transform.position, transform.position) - AimSwitchDif;

        foreach (PlayerCharacter target in TargetList)
        {
            float dist = Vector3.Distance(target.transform.position, transform.position);
            if (dist < minDist)
            {
                newTarget = target.gameObject;
                minDist = dist;
            }
        }

        return newTarget;
    }

    private GameObject PickRandomTarget()
    {
        GameObject result = null;

        List<PlayerCharacter> livingPlayers = new List<PlayerCharacter>();

        foreach (PlayerCharacter player in TargetList)
        {
            if (player != null && !player.IsDead)
            {
                livingPlayers.Add(player);
            }
        }

        if (livingPlayers.Count > 0)
            result = livingPlayers.GetRandom().gameObject;

        return result;
    }


    private void Shoot()
    {
        //TODO this is just temporary solution to enemy not shooting the dead player
        if (Target.GetComponent<EntityStats>().IsDead())
            return;

        //Debug.Log($"Shoot with {EnemyGun}");
        if (EnemyGun == null)
            return;

        if (EnemyGun.CanShoot() && direction.magnitude > 0)
        {
            PlaySFX(true);
            animator.Shoot(Vector3.forward, EnemyGun.Cooldown);
            EnemyGun.Fire(direction, false);
        }
        else
        {
            if (EnemyGun.TimeToNextShot > ReflexTime || direction.magnitude == 0)
            {
                direction = Target.transform.position - transform.position;
                direction = new Vector3(direction.x, 0, direction.z);
                direction.Normalize();
            }

            transform.rotation = Quaternion.LookRotation(direction);
        }
    }

    private bool IsInShootingDistance()
    {
        // Check if we're on the same Y level as the player.
        if (MathF.Abs(Target.transform.position.y - transform.position.y) > 5f)
        {
            return false;
        }

        Ray ray = new Ray(EnemyGun.transform.position, (Target.transform.position + AimingOrigin) - EnemyGun.transform.position);
        RaycastHit hit;

        Debug.DrawLine(ray.origin, ray.direction, Color.yellow);

        LayerMask mask = LayerMask.GetMask("Default", "Player");

        if (Physics.Raycast(ray, out hit, AimDistance, mask, QueryTriggerInteraction.Ignore))
        {

            //draw invisible ray cast/vector
            Debug.DrawLine(ray.origin, hit.point, Color.magenta);

            if (hit.collider.GetComponent<PlayerCharacter>() == null)
                return false;



            float distance = hit.distance;

            //Debug.Log($"distance: {distance}, hit: {hit.collider.gameObject}");

            return distance < AimDistance;
        }


        return false;
    }

    public void SetHostile(bool hostile)
    {
        TargetHostile = hostile;
    }

    void PlaySFX(bool shoot)
    {
        string path = "event:/SFX/sniperrifle";
        if (!shoot)
        {
            path = "event:/SFX/enemy-death";
        }
        sfx = FMODUnity.RuntimeManager.CreateInstance(path);
        sfx.start();
        sfx.release();
        sfx.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject.transform));
    }
}
