using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleSpawn : MonoBehaviour
{
    public GameObject EnemyPrefab;
    public bool SettupDetectionSphere = true;
    public float DetectionDistance = 13f;

    [SerializeField, ReadOnly]
    private GameObject enemyInstance;

    [HideInInspector]
    public GameObject EnemyInstance => enemyInstance;


    public GameObject ReSpawn()
    {
        if(enemyInstance != null)
        {
            Destroy(enemyInstance);
        }

        enemyInstance = Instantiate(EnemyPrefab, transform.position, transform.rotation, transform);

        GameObject detector = new GameObject("DetectionSphere");
        detector.layer = 2; // Ignore Raycast

        SphereCollider col = detector.AddComponent<SphereCollider>();
        col.radius = DetectionDistance;
        col.isTrigger = true;

        OnPlayerEnterTrigger trigger = detector.AddComponent<OnPlayerEnterTrigger>();
        trigger.playerMask = LayerMask.GetMask("Player");
        trigger.DisableOnTrigger = true;
        trigger.AnyPlayer = true;

        if(SettupDetectionSphere)
        {

            BraindeadAI ai = enemyInstance.GetComponent<BraindeadAI>();
            if(ai != null)
            {
                ai.TargetHostile = false;
                trigger.OnTrigger.AddListener(()=>ai.SetHostile(true));
            }

            TurretAI turret = enemyInstance.GetComponent<TurretAI>();
            if (turret != null)
            {
                turret.TargetHostile = false;
                trigger.OnTrigger.AddListener(delegateSetHostile);
            }

        }

        detector.transform.parent = enemyInstance.transform;
        detector.transform.localPosition = Vector3.zero;

        return enemyInstance;
    }

    private void delegateSetHostile()
    {
        if (enemyInstance == null)
            return;
        BraindeadAI ai = enemyInstance.GetComponent<BraindeadAI>();
        if (ai != null)
            ai.SetHostile(true);
    }

    private void OnDrawGizmos()
    {
        Matrix4x4 rotationMatrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);
        Gizmos.matrix = rotationMatrix;

        Gizmos.color = Color.red;
        Gizmos.DrawCube(Vector3.forward * 0.4f, Vector3.one * 0.2f);
        Gizmos.color = Color.magenta;
        Gizmos.DrawCube(Vector3.zero, Vector3.one * 0.5f);
    }

    private void OnDrawGizmosSelected()
    {

        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, DetectionDistance);
    }
}
