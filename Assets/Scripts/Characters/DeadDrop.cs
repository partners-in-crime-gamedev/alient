using UnityEngine;

[RequireComponent(typeof(EntityStats))]
public class DeadDrop : MonoBehaviour
{
    public GameObject DeadDropPrefab;
    [Range(0f, 1f)]
    public float DropChance;


    private void Awake()
    {
        GetComponent<EntityStats>().OnDeath.AddListener(DropItem);
    }

    private void DropItem(object sender, OnDeathEventArgs args)
    {
        float chance = Random.Range(0f, 1f);

        Debug.Log($"DeadDrop[{DeadDropPrefab.name}]: DropChance={DropChance}, random={chance}");

        if (chance < DropChance)
            Instantiate(DeadDropPrefab,transform.position,transform.rotation);
    }
}
