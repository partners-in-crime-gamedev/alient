using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    [SerializeField]
    private Bullet Bullet;
    [SerializeField]
    private Transform Barrel;
    [SerializeField]
    public float Cooldown;
    [SerializeField]
    private float BulletForce;

    public Vector3 BarrelPosition => Barrel?.position ?? Vector3.zero;

    [SerializeField] private GameObject GunFlashParticles;

    public float BulletDamage;

    public float MaxAngleDiviation = 0;

    public int ProjectileCount = 1;
    public float ProjectileGapAngle = 0.01f;
    public float ProjectileLifespan = 10f;

    [Header("Magazine")]
    public int MagazineCapacity = 7;
    public int RemainingBullets;
    public float ReloadTime = 1f;
    [ReadOnly] public float ReloadCounter;
    public float IdleTimeReload = 10f;
    [ReadOnly] public float IdleReloadCounter;
    public float TimeToNextShot => _counter;

    public bool Shotgun;

    private float _counter = 0f;

    private FMOD.Studio.EventInstance sfx;

    private void Start()
    {
        RemainingBullets = MagazineCapacity;
    }

    private void Update()
    {
        if (_counter > 0)
        {
            _counter -= Time.deltaTime;
        }
        if (ReloadCounter > 0)
        {
            ReloadCounter -= Time.deltaTime;
            if(ReloadCounter <= 0)
            {
                Reload();
                IdleReloadCounter = 0;
            }
        }
        if(IdleReloadCounter > 0)
        {
            IdleReloadCounter -= Time.deltaTime;
            if(IdleReloadCounter <= 0)
            {
                Reload();
            }
        }
    }

    public void Reload()
    {
        RemainingBullets = MagazineCapacity;

    }

    public void Aim(Vector3 direction)
    {
        this.transform.LookAt(this.transform.position + direction);
    }

    public void Fire(Vector3 direction, bool isPlayer)
	{
        Aim(direction);
        if (Bullet != null && CanShoot())
		{
            PlaySFX();

            float mainDiviation = RandomExtention.NormalRange(-MaxAngleDiviation, MaxAngleDiviation);
            float[] diviations = new float[ProjectileCount];
            Vector3[] directions = new Vector3[ProjectileCount];
            Quaternion[] rotations = new Quaternion[ProjectileCount];
            for (int i = 0; i < ProjectileCount; i++)
            {

                directions[i] = Quaternion.AngleAxis(mainDiviation - ProjectileGapAngle * (ProjectileCount/2) + ProjectileGapAngle * i, Vector3.up) * direction;
                rotations[i] = Quaternion.LookRotation(directions[i], Vector3.up);

                _counter = Cooldown;
                Bullet bullet = Instantiate(Bullet, Barrel.transform.position, rotations[i]);
                bullet.IsPlayerBullet = isPlayer;
                bullet.Damage = BulletDamage;
                bullet.transform.rotation = Quaternion.Euler(90, bullet.transform.rotation.eulerAngles.y, 0);
                bullet.Shoot(directions[i], BulletForce, ProjectileLifespan);
            }

            RemainingBullets--;
            IdleReloadCounter = IdleTimeReload;

            if(GunFlashParticles)
                Instantiate(GunFlashParticles, Barrel.transform.position, this.transform.rotation);
        }

        if(RemainingBullets == 0)
        {
            ReloadCounter = ReloadTime;
        }
	}

    public void PlaySFX() 
    {
        string path = "event:/SFX/gunshot";

        if (Shotgun)
        {
            path = "event:/SFX/shotgun";
        }
        sfx = FMODUnity.RuntimeManager.CreateInstance(path);
        sfx.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject.transform));
        sfx.start();
        sfx.release();
    }

    internal bool CanShoot()
    {
        return ReloadCounter <= 0 && _counter <= 0 && NoObstacle();
    }

    public bool NoObstacle()
    {

        LayerMask mask = LayerMask.GetMask("Default");


        if(Physics.Linecast(transform.position,Barrel.transform.position,mask))
        {
            return false;
        }

        return true;
    }
}
