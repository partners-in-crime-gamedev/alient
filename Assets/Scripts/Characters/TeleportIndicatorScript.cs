using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportIndicatorScript : MonoBehaviour
{
	[SerializeField]
	private bool displayIndicator;
	public bool DisplayIndicator => displayIndicator;
    [SerializeField, ColorUsage(false, true)] public Color _displayColor;
    [SerializeField, ColorUsage(false, true)] public Color _displayOnCooldown;
    [SerializeField, ReadOnly]private Material _instantiatedMaterial;

    private void Awake()
    {
        SetDisplaying(displayIndicator);

        ParticleSystem[] particles = GetComponentsInChildren<ParticleSystem>(true);
        foreach (ParticleSystem particle in particles)
        {
            var settings = particle.GetComponent<ParticleSystemRenderer>();
            if (!_instantiatedMaterial)
            {
                _instantiatedMaterial = settings.material;
            }
            settings.sharedMaterial = _instantiatedMaterial;
        }
    }


    public void SetColor(Color color)
	{
        ParticleSystem[] particles = GetComponentsInChildren<ParticleSystem>(true);
		foreach(ParticleSystem particle in particles)
        {
			ParticleSystem.MainModule settings = particle.main;
			settings.startColor = new ParticleSystem.MinMaxGradient(color);
		}
	}

	public void SetDisplaying(bool display)
	{
		displayIndicator = display;
		foreach (Transform child in transform)
		{
			child.gameObject.SetActive(displayIndicator);
		}
	}

    internal void SetAlpha(float v)
	{
		ParticleSystem[] particles = GetComponentsInChildren<ParticleSystem>(true);
		foreach (ParticleSystem particle in particles)
		{
			ParticleSystem.MinMaxGradient gradient = particle.main.startColor;
			Color color = gradient.color;
			color.a = v;
			ParticleSystem.MainModule settings = particle.main;
			settings.startColor = new ParticleSystem.MinMaxGradient(color);
		}
	}

    internal void SetEmission(float v)
    {
		Debug.Log(v);
        _instantiatedMaterial.SetColor("_EmissionColor", v>0.5f?_displayColor : _displayOnCooldown);
    }

	private void OnValidate()
    {
        SetDisplaying(DisplayIndicator);
    }
}
