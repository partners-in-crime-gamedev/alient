using System.Collections;
using UnityEngine;

public class RagdollBody : MonoBehaviour
{
    Rigidbody[] componentsInChildren;

    void Awake()
    {
        transform.SetParent(null);
        componentsInChildren = GetComponentsInChildren<Rigidbody>();
        StartCoroutine(SlowDown());
    }

    private IEnumerator SlowDown()
    {
        for (int i = 0; i < 5; i++)
        {
            yield return new WaitForSeconds(1f);
            MultiplyMass(1.5f);
        }
        foreach (var rigidbody in componentsInChildren)
        {
            var joint = rigidbody.GetComponent<Joint>();
            if (joint)
            {
                Destroy(joint);
            }
            Destroy(rigidbody);
        }
    }

    void MultiplyMass(float multiplier)
    {
        foreach (var rigidbody in componentsInChildren)
        {
            rigidbody.mass *= multiplier;
            rigidbody.drag = 0.8f;
        }
    }
}