using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public sealed class OnDeathEventArgs
{
    public GameObject MurderVictim;
}

[RequireComponent(typeof(OnHitScript))]
public class EntityStats : MonoBehaviour
{
    public UnityEvent<object, OnDeathEventArgs> OnDeath = new UnityEvent<object, OnDeathEventArgs>();
    public UnityEvent<object, OnHitEventArgs> OnDamage = new UnityEvent<object, OnHitEventArgs>();

    public float MaxHealth;
	public float CurrentHealth;
    public bool IsPlayer;

    private void Start()
    {
        CurrentHealth = MaxHealth;
        GetComponent<OnHitScript>().OnHit.AddListener(GetHit);
    }

    public bool IsDead()
    {
        return CurrentHealth <= 0;
    }

    bool died = false;
    public void GetHit(object sender, OnHitEventArgs e)
    {
        //Debug.Log($"{this}: got hit by {sender} with {e.PlayerBullet}-{e.Damage}");

        if(IsDead() && !died)
        {
            try
            {
                OnDeath.Invoke(sender, new OnDeathEventArgs() { MurderVictim = this.gameObject });
                SetRigidBodies(e);
                died = true;
            }
            catch (Exception ex)
            {
                if (ex != null)
                {
                    Destroy(gameObject);
                }
            }
            return;
        }


        if (!IsDead() && e.PlayerBullet != IsPlayer)
        {
			CurrentHealth -= e.Damage;
            OnDamage.Invoke(sender, e);
            if (IsDead())
            {
                CurrentHealth = 0;
                OnDeath.Invoke(sender, new OnDeathEventArgs() { MurderVictim = this.gameObject });
                died = true;

                SetRigidBodies(e);
            }
        }
    }

    private void SetRigidBodies(OnHitEventArgs e)
    {
        if (e.Origin != Vector3.zero)
        {
            Rigidbody[] rigidbodies = GetComponentsInChildren<Rigidbody>();

            foreach (Rigidbody rigidbody in rigidbodies)
            {
                rigidbody.isKinematic = false;
                rigidbody.AddExplosionForce(e.Force, e.Origin, 10);
            }

            if (!IsPlayer)
            {

                RagdollBody ragdollBody = gameObject.GetComponent<RagdollBody>();

                if (ragdollBody == null)
                    gameObject.AddComponent<RagdollBody>();

            }
        }
    }

    internal void GetHealed(float health)
    {
        CurrentHealth = Math.Clamp(CurrentHealth + health, 0, MaxHealth);
    }

    internal bool IsHurt()
    {
        return CurrentHealth < MaxHealth;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawSphere(this.transform.position + Vector3.up * 3, (CurrentHealth / MaxHealth) * 0.5f);
    }

    public void ResetHealth()
    {
        CurrentHealth = MaxHealth;
        died = false;
    }
}