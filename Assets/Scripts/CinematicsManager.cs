using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.Playables;
using UnityEditor;
namespace Speedrun
{
    [RequireComponent(typeof(PlayableDirector))]
    public class CinematicsManager : MonoBehaviour
    {
        private static CinematicsManager _instance;

        public static CinematicsManager Instance
        {
            get
            {
                if (!_instance)
                {
                    _instance = FindObjectOfType<CinematicsManager>(true);
                }

                return _instance;
            }
        }

        [SerializeField] private TMP_Text skipText;
        [SerializeField] public PlayableAsset IntroCutScene;
        [SerializeField] public PlayableAsset FadeToMain;

        private PlayableDirector director;

        public float cooldown = 1f;
        [SerializeField, ReadOnly]private float time = 0f;
        [SerializeField, ReadOnly] private bool clickedOnce = false;

        private void Skip()
        {
            if (!director)
            {
                director = GetComponent<PlayableDirector>();
            }
            director.time = director.duration - double.Epsilon;
            director.Evaluate();
            director.Play(FadeToMain);
        }

        // Start is called before the first frame update
        private void Start()
        {
            director = GetComponent<PlayableDirector>();
            if (MainMenuManager.StartedFromMenu)
            {
                director.Play(IntroCutScene);
            }
        }

        // Update is called once per frame
        private void Update()
        {
            if (director.state == PlayState.Playing)
            {
                if (clickedOnce)
                {
                    if (time <= 0f)
                    {
                        clickedOnce = false;
                        skipText.DOColor(Color.clear, 0.3f);
                    }
                    time -= Time.deltaTime;
                }
            }
            else if (clickedOnce)
            {
                clickedOnce = false;
                skipText.DOColor(Color.clear, 0.3f);
            }
        }

        public static void TriggerSkip()
        {
            var cm = Instance;
            if(!cm.clickedOnce || cm.time < cm.cooldown - 0.1f)
                cm.triggerSkip();
        }

        private void triggerSkip()
        {
            if (clickedOnce)
            {
                skipText.DOColor(Color.clear, 0.3f);
                Skip();
                clickedOnce = false;
            }
            else
            {
                clickedOnce = true;
                time = cooldown;
                skipText.DOColor(Color.white, 0.3f);
                //skipText.material.DOFade(1.0f, "_FaceColor", 0.3f);
                //skipText.material.DOFade(1.0f, 0.3f);
            }
        }

        public static bool IsPlaying()
        {
            return Instance?.director?.state == PlayState.Playing;
        }
    }
}
