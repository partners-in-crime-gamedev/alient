using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class PauseManager : MonoBehaviour
{

	public static bool IsPaused { get; private set; }
	private static float _prevTimescale;

	public UnityEvent OnPauseEvent = new UnityEvent();
	public UnityEvent OnUnPauseEvent = new UnityEvent();

	private static float _btnCooldown;
	private static PauseManager _instance;

	public static PauseManager Instance
	{
		get
		{
			if (!_instance)
			{
				_instance = FindObjectOfType<PauseManager>();
			}
			return _instance;
		}
	}


	// Start is called before the first frame update

	void Start()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnSceneLoaded(Scene arg0, LoadSceneMode arg1)
    {
		Time.timeScale = 1f;
    }

	// Update is called once per frame

	void Update()
    {
	    if (_btnCooldown > 0)
	    {
		    _btnCooldown -= Time.unscaledDeltaTime;
	    }
    }

    public static void PauseGame()
    {
	    if (_btnCooldown <= 0)
	    {
		    _btnCooldown = 0.001f;
		    if (IsPaused)
		    {
			    IsPaused = false;
			    Time.timeScale = _prevTimescale;
				Instance?.OnUnPauseEvent?.Invoke();
		    }
		    else
		    {
			    IsPaused = true;
			    _prevTimescale = Time.timeScale;
			    Time.timeScale = 0;
				Instance?.OnPauseEvent?.Invoke();
		    }
		}
    }

    public static void Resume()
    {
        if (_prevTimescale > 0)
        {
            Time.timeScale = _prevTimescale;
        }
        else
        {
            Time.timeScale = 1f;
        }
		IsPaused = false;
        Instance?.OnUnPauseEvent?.Invoke(); 
        _btnCooldown = 0.001f;
    }
}
