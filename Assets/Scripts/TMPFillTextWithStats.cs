using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

/*
* Created by webik150
*/
[DefaultExecutionOrder(-100), RequireComponent(typeof(TMP_Text))]
public class TMPFillTextWithStats : MonoBehaviour
{
    private TMP_Text text;

    [SerializeField] private string KillsKey;
    [SerializeField] private string DeathsKey;
    // Start is called before the first frame update
    void OnEnable()
    {
        text = GetComponent<TMP_Text>();
        text.text = text.text.Replace(KillsKey, StatsManager.Kills.ToString()).Replace(DeathsKey, StatsManager.Deaths.ToString());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
