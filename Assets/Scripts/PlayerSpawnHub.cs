using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawnHub : PlayerSpawn
{
	[SerializeField] private LockableDoors plasmaDoorsInside;
	[SerializeField] private LockableDoors plasmaDoorsOutside;
	[SerializeField] private LockableDoors shieldDoorsInside;
	[SerializeField] private LockableDoors shieldDoorsOutside;
	[SerializeField] private LockableDoors blinkDoorsInside;
	[SerializeField] private LockableDoors blinkDoorsOutside;

	// Start is called before the first frame update
	protected virtual void OnEnable()
	{
		base.OnRespawn.AddListener(SetDoorState);
	}

	// Update is called once per frame

	protected virtual void OnDisable()
	{
		base.OnRespawn.RemoveListener(SetDoorState);
	}

	public void SetDoorState()
	{
		var player = FindObjectOfType<PlayerCharacter>();
		if (player)
		{
			if (player.HasShield)
			{
				shieldDoorsInside.LockDoors();
				shieldDoorsOutside.LockDoors();
			}
			else
			{
				shieldDoorsInside.UnlockDoors();
				shieldDoorsOutside.UnlockDoors();
			}

			if (player.HasBlink)
			{
				blinkDoorsInside.LockDoors();
				blinkDoorsOutside.LockDoors();
			}
			else
			{
				blinkDoorsInside.UnlockDoors();
				blinkDoorsOutside.UnlockDoors();
			}

			if (player.HasPlasma)
			{
				plasmaDoorsInside.LockDoors();
				plasmaDoorsOutside.LockDoors();
			}
			else
			{
				plasmaDoorsInside.UnlockDoors();
				plasmaDoorsOutside.UnlockDoors();
			}
		}
	}
}
