using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destructible : MonoBehaviour
{

    public GameObject ObjectToSpawn;
    public bool InheritScale;

    private GameObject _spawnedObject;



    // Start is called before the first frame update
    void Awake()
    {
        PlayerManager.OnGameOver.AddListener(Respawn);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Destruct()
    {
        _spawnedObject = Instantiate(ObjectToSpawn);
        _spawnedObject.transform.position = transform.position;
        _spawnedObject.transform.rotation = transform.rotation;
        if (InheritScale)
        {
            _spawnedObject.transform.localScale = transform.localScale;
        }
        gameObject.SetActive(false);
    }

    public void Respawn()
    {
        gameObject.SetActive(true);
    }

    public void OnEnable()
    {
        if (_spawnedObject)
        {
            Destroy(_spawnedObject);
        }
    }
}
