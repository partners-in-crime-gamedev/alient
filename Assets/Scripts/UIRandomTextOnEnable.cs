using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

/*
* Created by webik150
*/
[RequireComponent(typeof(TMP_Text)), DefaultExecutionOrder(-100)]
public class UIRandomTextOnEnable : MonoBehaviour
{

    [SerializeField] private List<string> _texts = new();
    // Start is called before the first frame update
    void OnEnable()
    {
        GetComponent<TMP_Text>().text = _texts.GetRandom();
    }
}
