using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityFx.Outline;

/*
* Created by webik150
*/

public class CutscenePlayerManager : MonoBehaviour
{

    [SerializeField] OutlineSettings _player1OutlineSettings;
    [SerializeField] private Renderer _player1Renderer;


    [SerializeField] OutlineSettings _player2OutlineSettings;
    [SerializeField] private Renderer _player2Renderer;
    [SerializeField] private GameObject _player2;

    // Start is called before the first frame update
    void OnEnable()
    {
        if (MainMenuManager.IsSinglePlayerGame)
        {
            _player2.SetActive(false);
            _player1Renderer.material.color = _player1OutlineSettings.OutlineColor;
        }
        else
        {
            _player2.SetActive(true);
            _player2Renderer.material.color = _player2OutlineSettings.OutlineColor;
            _player1Renderer.material.color = _player1OutlineSettings.OutlineColor;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
