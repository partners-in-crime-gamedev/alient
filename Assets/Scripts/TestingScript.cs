using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestingScript : MonoBehaviour
{
    public List<GameObject> portLocations = new List<GameObject>();


    private void OnGUI()
    {
        //if (GUILayout.Button("something"))
        //{
        //    Debug.Log("Pressed");
        //}


        for (int i = 0; i < portLocations.Count; i++)
        {
            if (GUI.Button(new Rect(30, 200 + 30*i, 100, 25), portLocations[i].name))
            {
                Teleport(portLocations[i]);
            }
        }


        if (GUI.Button(new Rect(30, 150, 100, 25), "Set The Sunshine"))
        {
            SetSunshite();
        }
    }

    private void SetSunshite()
    {
        PlayerCharacter[] players = FindObjectsOfType<PlayerCharacter>();

        foreach(PlayerCharacter player in players)
        {
            Gun gun = player.Gun;
            if (gun != null)
            {
                gun.Cooldown = 0.3f;
                gun.BulletDamage = 100f;
                gun.ProjectileCount = 36;
                gun.ProjectileGapAngle = 10;
                gun.ReloadTime = 0;
            }

            player.BurstMode = BurstFireMode.FullAuto;

            EntityStats stats = player.GetComponent<EntityStats>();
            if (stats != null)
            {
                stats.MaxHealth = 10000;
                stats.ResetHealth();
            }
        }
    }

    private void Teleport(GameObject go)
    {
        PlayerCharacter character = FindObjectOfType<PlayerCharacter>();

        PlayerCamera camera = FindObjectOfType<PlayerCamera>();

        CharacterController controller = character.GetComponent<CharacterController>();

        controller.enabled = false;

        character.transform.position = go.transform.position;

        camera.ResetPosition();

        controller.enabled = true;

    }
}
