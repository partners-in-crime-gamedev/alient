using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
* Created by webik150
*/

[RequireComponent(typeof(CanvasScaler))]
public class UIScaler : MonoBehaviour
{
    private CanvasScaler scaler;
    // Start is called before the first frame update
    void Start()
    {
        scaler = GetComponent<CanvasScaler>();
    }

    // Update is called once per frame
    void Update()
    {
        var factor = Screen.width / 1920f;
        if (factor >= 1.8f)
        {
            scaler.scaleFactor = Mathf.Floor(factor);
        }
        else
        {
            scaler.scaleFactor = 1f;
        }
    }
}
