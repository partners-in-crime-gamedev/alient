using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
* Created by webik150
*/

public class StatsManager : MonoBehaviour
{
    public static int Deaths { get; private set; }

    public static int Kills { get; private set; }
    // Start is called before the first frame update
    void Start()
    {
        Deaths = 1;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static void AddKill()
    {
        Kills++;
    }

    public static void AddDeath()
    {
        Deaths++;
    }
}
