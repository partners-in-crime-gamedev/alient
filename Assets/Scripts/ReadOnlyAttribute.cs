using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Value is visible in Inspector, but cannot be eddited
/// </summary>
[AttributeUsage(AttributeTargets.Field|AttributeTargets.Property)]
public class ReadOnlyAttribute : PropertyAttribute
{

}