using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameState : MonoBehaviour
{
    #region Singleton Initiation

    private static GameState _instance;

	public static GameState Instance
	{
		get
        {
			if (_instance == null)
            {
                if (_instance != null) return _instance;
                _instance = FindObjectOfType<GameState>();
                if (_instance == null)
                {
                    GameObject go = new GameObject();
                    go.name = nameof(GameState);
                    _instance = go.AddComponent<GameState>();
                }
                GameObject.DontDestroyOnLoad(_instance);
            }
			return _instance;
        }
	}

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this);
        }
        else
        {
            _instance = this;
        }
    }

    #endregion

    public bool PlasmaTaken;
    public bool BlinkTaken;
    public bool ShieldTaken;

    public void ResetAbilities()
    {
        PlasmaTaken = false;
        BlinkTaken = false;
        ShieldTaken = false;
    }

    internal void TakeAbility(AbilityType abilityType)
    {
        switch (abilityType)
        {
            case AbilityType.Plasma:
                PlasmaTaken = true;
                break;
            case AbilityType.Shield:
                ShieldTaken = true;
                break;
            case AbilityType.Blink:
                BlinkTaken = true;
                break;
            default:
                break;
        }
    }
}
