using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// After it's been too high for specified time it is instantly shot down by anti-air defense and obliterated with no remains faster then new frame is drawn.
/// </summary>
public class AntiAirDefense : MonoBehaviour
{
    public float Countdown = 5f;
    [ReadOnly]
    public float Counter;
    public float MaxHeight = 5f;

    [ReadOnly]
    public bool DangerZone = false;

    private void Update()
    {
        if(DangerZone)
        {
            if(transform.position.y < MaxHeight)
            {
                DangerZone = false;
                Counter = 0;
            }
            else
            {
                Counter -= Time.deltaTime;
                if(Counter < 0)
                {
                    EntityStats[] stats = GetComponentsInChildren<EntityStats>();
                    foreach(EntityStats item in stats)
                    {
                        if(!item.IsDead())
                            item.OnDeath.Invoke(this, new OnDeathEventArgs() { MurderVictim = item.gameObject });
                    }
                    Destroy(gameObject);
                }
            }
        }
        else
        {

            if(transform.position.y >= MaxHeight)
            {
                DangerZone = true;
                Counter = Countdown;
            }

        }
    }
}
