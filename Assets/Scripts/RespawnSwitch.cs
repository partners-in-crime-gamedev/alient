using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class RespawnSwitch : MonoBehaviour
{
    public int newSpawnIndex = 0;

    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<PlayerCharacter>() != null)
        {
            FindObjectOfType<RespawnManager>().ChangeCurrentSpawnIndex(newSpawnIndex);
        }
    }
}
