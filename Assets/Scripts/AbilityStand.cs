using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public enum AbilityType
{
	Plasma,
	Blink,
	Shield
}

public class AbilityStand : MonoBehaviour
{
	[SerializeField]
	public GameObject AbilityModel;

	public AbilityType AbilityType;

    private void Start()
    {
		Sequence seq = DOTween.Sequence();
		seq.Append(AbilityModel.transform.DORotate(new Vector3(0, 90, 0), 2.5f)
			.SetLoops(4,LoopType.Incremental)
			.SetEase(Ease.Linear));
		seq.Join(AbilityModel.transform.DOMoveY(AbilityModel.transform.position.y + 0.2f, 1f)
			.SetLoops(10,LoopType.Yoyo)
			.SetEase(Ease.InOutQuad));

		seq.SetLoops(-1,LoopType.Restart);

    }

    private void OnTriggerEnter(Collider other)
    {
		if (other.GetComponent<PlayerCharacter>() == null)
			return;

			
		GameState.Instance.TakeAbility(AbilityType);


		PlayerCharacter[] characters = FindObjectsOfType<PlayerCharacter>();
		PlayerCamera camera = FindObjectOfType<PlayerCamera>();

        foreach (PlayerCharacter character in characters)
        {
			character.GainAbility(AbilityType);
			camera.GainAbility(AbilityType);
		}

		AbilityModel.SetActive((false));


		this.gameObject.SetActive(false);
    }
}
