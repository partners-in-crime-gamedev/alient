using System.Collections;
using System.Linq;
using TMPro;
using UnityEngine;

namespace Speedrun
{
    [RequireComponent(typeof(TMP_Dropdown))]
    public class QualityDropdown : MonoBehaviour
    {
        private TMP_Dropdown dropdown;

        [SerializeField] private GameObject _globalVolume;
        // Start is called before the first frame update
        void Awake()
        {
            dropdown = GetComponent<TMP_Dropdown>();
            dropdown.options.Clear();
            dropdown.options.AddRange(QualitySettings.names.Select(q=>new TMP_Dropdown.OptionData(q)));
            dropdown.SetValueWithoutNotify(QualitySettings.GetQualityLevel());
        }

        // Update is called once per frame
        void Update()
        {
        
        }

        public void OnChange(int index)
        {
            SettingsManager.SaveQuality(index);
            StartCoroutine(ChangeQuality(index));
        }

        private IEnumerator ChangeQuality(int index)
        {
            QualitySettings.SetQualityLevel(index, true);
            _globalVolume.SetActive(false);
            yield return null;
            _globalVolume.SetActive(true);
        }
    }
}
