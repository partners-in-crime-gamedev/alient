using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Events;

/*
 * Created by webik150
 */

namespace Speedrun
{
    public class SettingsManager: MonoBehaviour
    {

       /* public UnityEvent<float> OnMasterLoad = new UnityEvent<float>();
        public UnityEvent<float> OnMusicLoad = new UnityEvent<float>();
        public UnityEvent<float> OnSFXLoad = new UnityEvent<float>();*/

        void Start()
        {
            LoadAll();
        }

        void OnDestroy()
        {
            Flush();
        }

        public static void Flush()
        {
            PlayerPrefs.Save();
        }

        public static void SaveFullscreen(bool fullscreen)
        {
            PlayerPrefs.SetInt("fullscreen", fullscreen?1:0);
        }

        public static void SaveQuality(int level)
        {
            PlayerPrefs.SetInt("quality", level);
        }
        public static void SaveResolution(int index)
        {
            PlayerPrefs.SetInt("resolution", index);
        }
        public static void SaveMasterVolume(float value)
        {
            PlayerPrefs.SetFloat("mastervolume", value);
        }
        public static void SaveMusicVolume(float value)
        {
            PlayerPrefs.SetFloat("musicvolume", value);
        }
        public static void SaveSFXVolume(float value)
        {
            PlayerPrefs.SetFloat("sfxvolume", value);

        }

        public static void LoadFullscreen()
        {
            var fullScreen = PlayerPrefs.GetInt("fullscreen", 1)==1;
            if(fullScreen != Screen.fullScreen)
                Screen.fullScreen = fullScreen;
        }

        public static void LoadQuality()
        {
            var qualityLevel = PlayerPrefs.GetInt("quality", 2);
            if(qualityLevel!=QualitySettings.GetQualityLevel())
                QualitySettings.SetQualityLevel(qualityLevel,true);
        }
        public static void LoadResolution()
        {
            var i = PlayerPrefs.GetInt("resolution", -1);
            if (i != -1)
            {
                var resolutions = FillResolutions.GetResolutions();
                var width = resolutions[i].width;
                var height = resolutions[i].height;
                Screen.SetResolution(width, height, Screen.fullScreenMode);
            }
        }
        public static void LoadMasterVolume()
        {
	        FindObjectOfType<AudioSettings>().MasterVolumeLevel(PlayerPrefs.GetFloat("mastervolume", 0.5f));
        }
        public static void LoadMusicVolume( )
        {
	        FindObjectOfType<AudioSettings>().MusicVolumeLevel(PlayerPrefs.GetFloat("musicvolume", 0.5f));
        }
        public static void LoadSFXVolume()
        {
	        FindObjectOfType<AudioSettings>().SFXVolumeLevel(PlayerPrefs.GetFloat("sfxvolume", 0.5f));
        }

        public static void LoadAll()
        {
            LoadAudio();
            LoadGraphics();
        }

        public static void LoadGraphics()
        {
            LoadFullscreen();
            LoadResolution();
            LoadQuality();
        }

        public static void LoadAudio()
        {
            LoadMasterVolume();
            LoadMusicVolume();
            LoadSFXVolume();
        }
    }
}
