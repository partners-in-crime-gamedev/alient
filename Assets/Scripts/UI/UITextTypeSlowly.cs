using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;

/*
* Created by webik150
*/
[DefaultExecutionOrder(-10),RequireComponent(typeof(TMP_Text))]
public class UITextTypeSlowly : MonoBehaviour
{
    private TMP_Text text;
    [SerializeField] private int minLettersPerSecond = 10;
    [SerializeField] private int maxLettersPerSecond = 10;
    [SerializeField,ReadOnly] private float minLetterInterval = 0f;
    [SerializeField,ReadOnly] private float maxLetterInterval = 0.1f;
    [SerializeField] private float delay = 0.0f;
    [SerializeField] private bool asSingleLine;
    private List<TextLine> lines = new List<TextLine>();

    private string targetText;

    private void OnValidate()
    {
        RecalculateIntervals();
    }

    // Start is called before the first frame update
    void OnEnable()
    {
        Debug.Log(gameObject.name + " is enabled");
        text = GetComponent<TMP_Text>();
        targetText = text.text;
        lines.Clear();
        text.text = "";

        if (delay <= 0f)
        {
            Init();
        }
        else
        {
            Invoke("Init", delay);
        }
    }

    private void Init()
    {
        RecalculateIntervals();
        lines.Clear();
        text.text = "";

        if (asSingleLine)
        {
            lines.Add(new TextLine(targetText, minLetterInterval, maxLetterInterval));
        }
        else
        {
            foreach (var s in targetText.Split('\n'))
            {
                lines.Add(new TextLine(s, minLetterInterval, maxLetterInterval));
            }
        }

    }

    private void RecalculateIntervals()
    {
        maxLetterInterval = 1 / (float)minLettersPerSecond;
        minLetterInterval = 1 / (float)maxLettersPerSecond;
    }

    void OnDisable()
    {
        text.text = targetText;
    }

    // Update is called once per frame
    void Update()
    {
        string str = gameObject.name + "\n" + text.text + "\n -> \n";

        foreach (var textLine in lines)
        {
            textLine.Update();
        }

        text.text = string.Join("\n", lines);

        //Debug.Log(str + text.text);
    }

    private class TextLine
    {
        private readonly Regex colorStartRegex = new Regex("<color=(.+?)>");
        private readonly Regex colorEndRegex = new Regex("</color>");
        private readonly Regex sizeStartRegex = new Regex("<size=(.+?)>");
        private readonly Regex sizeEndRegex = new Regex("</size>");

        sealed class TagArea
        {
            public string Code;
            public int Offset;
            public int StartIndex;
            public int EndIndex;
            public int Length;
        }

        public string line;
        public string lineTarget;
        public int letterIndex = 0;
        public float timeToNextLetter = 0;
        private float minLetterInterval;
        private float maxLetterInterval;
        private Queue<TagArea> colors = new Queue<TagArea>();
        private Queue<TagArea> sizes = new Queue<TagArea>();

        public TextLine(string line, float minLetterInterval, float maxLetterInterval)
        {
            this.line = "";
            timeToNextLetter = Random.Range(minLetterInterval, maxLetterInterval);
            this.minLetterInterval = minLetterInterval;
            this.maxLetterInterval = maxLetterInterval;
            this.lineTarget = line;
            var startMatch = colorStartRegex.Match(line);
            var endMatch = colorEndRegex.Match(line);

            while (letterIndex < line.Length)
            {
                if (startMatch.Success)
                {
                    if (endMatch.Success)
                    {
                        letterIndex = endMatch.Index + endMatch.Length;
                    }
                    else
                    {
                        letterIndex = line.Length;
                    }
                    colors.Enqueue(
                        new TagArea
                        {
                            Code = startMatch.Groups[1].Value,
                            StartIndex = startMatch.Index,
                            Offset = startMatch.Length,
                            Length = letterIndex - startMatch.Index,
                            EndIndex = endMatch.Success ? endMatch.Index : letterIndex
                        });
                    startMatch = startMatch.NextMatch();
                    endMatch = endMatch.NextMatch();
                }
                else
                {
                    break;
                }
            }

            letterIndex = 0;


            startMatch = sizeStartRegex.Match(line);
            endMatch = sizeEndRegex.Match(line);

            while (letterIndex < line.Length)
            {
                if (startMatch.Success)
                {
                    if (endMatch.Success)
                    {
                        letterIndex = endMatch.Index + endMatch.Length;
                    }
                    else
                    {
                        letterIndex = line.Length;
                    }
                    sizes.Enqueue(
                        new TagArea
                        {
                            Code = startMatch.Groups[1].Value,
                            StartIndex = startMatch.Index,
                            Offset = startMatch.Length,
                            Length = letterIndex - startMatch.Index,
                            EndIndex = endMatch.Success ? endMatch.Index : letterIndex
                        });
                    startMatch = startMatch.NextMatch();
                    endMatch = endMatch.NextMatch();
                }
                else
                {
                    break;
                }
            }

            letterIndex = 0;
        }


        float writeTime = 0;

        public void Update()
        {
            if(letterIndex > lineTarget.Length)
            {
                line = lineTarget;
                return;
            }

            writeTime += Time.deltaTime;
            while(writeTime > 0)
            {
                writeTime -= timeToNextLetter;
                timeToNextLetter = Random.Range(minLetterInterval, maxLetterInterval);
                letterIndex++;
            }
            if (writeTime <= 0)
            {
                if (letterIndex < lineTarget.Length)
                {
                    if (colors.Count > 0)
                    {
                        if (letterIndex >= colors.Peek().EndIndex - colors.Peek().Offset)
                        {
                            var area = colors.Dequeue();
                            var prevI = letterIndex;
                            letterIndex = area.StartIndex + area.Length;
                            line = new string(lineTarget.Take(letterIndex + 1).ToArray());
                        }
                        else if (letterIndex >= colors.Peek().StartIndex)
                        {
                            line = new string(lineTarget.Take(colors.Peek().Offset + letterIndex +1).ToArray()) + "</color>";
                            //letterIndex++;
                        }
                        else
                        {
                            line = new string(lineTarget.Take(letterIndex + 1).ToArray());
                            //letterIndex++;
                        }
                    }
                    else
                    {
                        line = new string(lineTarget.Take(letterIndex + 1).ToArray());
                        //letterIndex++;
                    }
                }
            }
        }

        public override string ToString()
        {
            return $"{line}{(!line.Equals(lineTarget) && ((int)Time.timeSinceLevelLoad)%2==0?"_":"")}";
        }
    }
}
