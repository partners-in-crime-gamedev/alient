using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class UIPlayerHUD : MonoBehaviour
{
	[SerializeField] private Slider _healthSlider;
	[SerializeField] private TMP_Text _currentAmmoText;
	[SerializeField] private TMP_Text _maxAmmoText;
	[SerializeField] private Graphic _abilityPlasmaBall;
	[SerializeField] private Graphic _abilityBlink;
	[SerializeField] private Graphic _abilityShield;
	[SerializeField] private Image _abilityPlasmaBallDial;
	[SerializeField] private Image _abilityBlinkDial;
	[SerializeField] private Image _abilityShieldDial;
	[SerializeField] private Renderer _playerPreview;
	[SerializeField] private Material uiMaterial;
	[SerializeField] private CanvasGroup _canvasGroup;
	[SerializeField] private Animator _ammoAnimator;
	private PlayerOutlineData _playerColor;
	private PlayerCharacter _playerCharacter;
	private EntityStats _stats;
	private bool _initizlized;
	[SerializeField] Color abilityEnabledColor = Color.white;
	[SerializeField] Color abilityDisabledColor = Color.gray;
	[SerializeField,ColorUsage(true,true)]private Color uiBrightness;

    [SerializeField] private GameObject _deadText;
    [SerializeField] private CanvasGroup _iconsGroup;
    [SerializeField] private CanvasGroup _healthGroup;
    private bool _reloadTriggered;

    // Start is called before the first frame update
	void Awake()
	{
		_canvasGroup.alpha = 0f;
	}

	// Update is called once per frame
	void Update()
	{

		if (_initizlized)
		{
            if (_playerCharacter.IsDead)
            {
                _iconsGroup.alpha = 0.25f;
                _healthGroup.alpha = 0.25f;
				_deadText.SetActive(true);
            }
            else
            {
				_deadText.SetActive(false);
                _iconsGroup.alpha = 1f;
                _healthGroup.alpha = 1f;
				_healthSlider.maxValue = _stats.MaxHealth;
                _healthSlider.value = _stats.CurrentHealth;
                _abilityBlink.color = _playerCharacter.HasBlink ? abilityEnabledColor : abilityDisabledColor;
                _abilityBlinkDial.fillAmount = _playerCharacter.HasBlink ? _playerCharacter.Blink.CooldownProgress : 0f;
                _abilityShield.color = _playerCharacter.HasShield ? abilityEnabledColor : abilityDisabledColor;
                _abilityShieldDial.fillAmount = _playerCharacter.HasShield ? _playerCharacter.Shield.CooldownProgress : 0f;
                _abilityPlasmaBall.color = _playerCharacter.HasPlasma ? abilityEnabledColor : abilityDisabledColor;
                _abilityPlasmaBallDial.fillAmount = _playerCharacter.HasPlasma ? _playerCharacter.PlasmaLauncher.CooldownProgress : 0f;
                _currentAmmoText.text = _playerCharacter.Gun.RemainingBullets.ToString();
                _maxAmmoText.text = $"/{_playerCharacter.Gun.MagazineCapacity}";
                if (_playerCharacter.Gun.RemainingBullets == 0 && !_reloadTriggered)
                {
                    _reloadTriggered = true;
                    OnReload();
                }

                if (_playerCharacter.Gun.RemainingBullets > 0 && _reloadTriggered)
                {
                    _reloadTriggered = false;
                }
			}
            
        }
	}

	public void SetupPlayer(GameObject player)
	{
		_stats = player.GetComponent<EntityStats>();
		_healthSlider.minValue = 0;
		_healthSlider.maxValue = _stats.MaxHealth;
		_healthSlider.value = _stats.CurrentHealth;
		_playerCharacter = player.GetComponent<PlayerCharacter>();
		_playerColor = player.GetComponent<PlayerOutlineData>();
		var color = _playerColor.playerRenderer.material.color;
		_playerPreview.material.color = color;
		uiMaterial.color = color * uiBrightness;
		_currentAmmoText.color = color;
		_maxAmmoText.color = color;
		_initizlized = true;
		_canvasGroup.alpha = 1.0f;
    }

    public void OnReload()
    {
		//Add to OnReload event in player gun
        _ammoAnimator.SetTrigger("reload");
    }
}
