using UnityEngine;
using UnityEngine.UI;

namespace Speedrun
{
    [RequireComponent(typeof(Toggle))]
    public class FullscreenToggle : MonoBehaviour
    {
        // Start is called before the first frame update
        void Awake()
        {
            GetComponent<Toggle>().SetIsOnWithoutNotify(Screen.fullScreen);
        }

        // Update is called once per frame
        void Update()
        {
        
        }

        public void ChangeFullscreen(bool value)
        {
            //Screen.fullScreen = value;
            Screen.SetResolution(Screen.width,Screen.height,value? FullScreenMode.ExclusiveFullScreen : FullScreenMode.Windowed);
            SettingsManager.SaveFullscreen(value);
        }
    }
}
