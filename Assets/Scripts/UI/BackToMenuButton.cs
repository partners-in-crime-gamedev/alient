using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class BackToMenuButton : MonoBehaviour
{
	[SerializeField] private string _menuSceneName;
    // Start is called before the first frame update
    void Start()
    {
        Assert.IsFalse(string.IsNullOrEmpty(_menuSceneName));
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void BackToMenu()
    {
	    foreach (var playerInput in FindObjectsOfType<PlayerInput>())
	    {
		    Destroy(playerInput.gameObject);
	    }
	    PauseManager.PauseGame();
        SceneManager.LoadScene(_menuSceneName);
    }
}
