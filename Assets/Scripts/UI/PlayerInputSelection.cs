using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

/*
* Created by webik150
*/

public class PlayerInputSelection : MonoBehaviour
{
    [SerializeField]private bool _singlePlayerMode;
    [SerializeField] private int _playerIndex;
	[SerializeField] private TMP_Text _messageLabel;
	[SerializeField] private Image _deviceImage;
	[SerializeField] private Sprite _gamepadSprite;
	[SerializeField] private Sprite _keyboardSprite;
	[SerializeField] private Sprite _noInputSprite;
	[SerializeField] private PlayerInputManager _inputManager;
	private PlayerInput _foundInput;
    GunLaser gunLaser;

	public bool InputDeviceFound => _foundInput != null;
	public string DetectingGamepadText = "Press any button to join";
	public string NoInputSelectedText = "No input selected";
	private string _inputDeviceFoundText = "<color=green>device found!</color>";


	private int _dotCounter = 0;
	private float _dotCounterTime = 0;

    [SerializeField, ReadOnly] private Color _selectedColor;
    [SerializeField] private Color[] _possibleColors = new Color[8];
    [SerializeField] private AlienColorTemplate _alienColorTemplate;
    [SerializeField] private RectTransform _colorsContainer;
    private List<AlienColorTemplate> _colorTemplates = new();
	private Material _playerMaterial;

	[SerializeField] private Transform _spawnPoint;
	[SerializeField] private Vector3 _offset;

	// Start is called before the first frame update
	void Start()
	{
		if (_messageLabel)
		{
			_messageLabel.text = NoInputSelectedText;
		}

		if (_deviceImage && _noInputSprite)
		{
			_deviceImage.sprite = _noInputSprite;
		}
	}

	// Update is called once per frame
	void Update()
	{
		if (_foundInput)
		{
			_messageLabel.text = _inputDeviceFoundText.Replace("device", string.Join(" & ", _foundInput.devices.Select(x => x.displayName)));
			_foundInput.transform.position = _spawnPoint.position + _offset;
			float rotationSpeed = 20f; //TODO: Allow rotation through controller
			_foundInput.transform.Rotate(Vector3.up, Time.deltaTime * rotationSpeed);

			if (_playerMaterial)
			{
				_playerMaterial.color = _selectedColor;
			}
			
            gunLaser.transform.parent.parent.localRotation = Quaternion.Euler(-94.795f, 40.6242676f, 82.81879f);
		}
		else
		{
			_messageLabel.text = DetectingGamepadText;
			for (int i = 0; i < _dotCounter; i++)
			{
				_messageLabel.text += ".";
			}

			if (_dotCounterTime <= 0)
			{
				_dotCounter++;
				_dotCounter %= 4;
				_dotCounterTime = 1f;
			}

			_dotCounterTime -= Time.deltaTime;
		}
	}

	void OnEnable()
	{
        DestroyPlayer();
		if (!_singlePlayerMode && !_inputManager.joiningEnabled)
		{
			_inputManager.EnableJoining();
		}

        if (_singlePlayerMode)
        {
			JoinSinglePlayer();
        }
	}

	void OnDisable()
	{
		if (_inputManager.joiningEnabled)
		{
			_inputManager.DisableJoining();
		}
    }

	public void DestroyPlayer()
	{
		if (_foundInput)
		{
			Destroy(_foundInput.gameObject);
			_foundInput = null;

            for (int i = _colorTemplates.Count - 1; i >= 0; i--)
            {
                Destroy(_colorTemplates[i].gameObject);
            }
            _colorTemplates.Clear();
		}
	}

	public void StartGame()
	{
		if (!_foundInput)
		{
			return; //Starting single player;
		}

		_foundInput.GetComponentInChildren<Animator>().SetBool("Idle", false);
        gunLaser.gameObject.SetActive(true);
		DontDestroyOnLoad(_foundInput.gameObject);
		_foundInput.ActivateInput();
	}

	public void PlayerLeft(PlayerInput input)
	{
		if (input.playerIndex == _playerIndex)
		{
            DestroyPlayer();
			if (_deviceImage && _noInputSprite)
			{
				_deviceImage.sprite = _noInputSprite;
			}
		}
	}
	public void PlayerJoined(PlayerInput input)
	{
		if (input.playerIndex == _playerIndex)
		{
			_foundInput = input;
			_foundInput.DeactivateInput();
			_foundInput.transform.position = _spawnPoint.position;
			_foundInput.transform.Rotate(Vector3.up, UnityEngine.Random.Range(0, 360));
			_foundInput.GetComponentInChildren<Animator>().SetBool("Idle", true);
            gunLaser = _foundInput.GetComponentInChildren<GunLaser>(true);
            gunLaser.gameObject.SetActive(false);
            var playerRenderer = _foundInput.GetComponentInChildren<SkinnedMeshRenderer>();
			_playerMaterial = playerRenderer.material;

			for (int i = 0; i < _possibleColors.Length; i++)
            {
                var alienColorTemplate = Instantiate(_alienColorTemplate, _colorsContainer);
                alienColorTemplate.Color = _possibleColors[i];
                _colorTemplates.Add(alienColorTemplate);
            }

			_colorTemplates.GetRandom().Select();
            _foundInput.deviceLostEvent.AddListener(PlayerLeft);
			if (input.devices.First() is Gamepad)
			{
				if (_deviceImage && _gamepadSprite)
				{
					_deviceImage.sprite = _gamepadSprite;
				}
			}
			else if (_deviceImage && _keyboardSprite)
			{
				_deviceImage.sprite = _keyboardSprite;
			}
            gunLaser.transform.parent.parent.localRotation = Quaternion.Euler(6.20499277f, 40.6242676f, 82.81879f);
        }
	}

    public void SetColor(Color color)
    {
        _selectedColor = color;
    }

    public void JoinSinglePlayer()
    {
        var player = _inputManager.JoinPlayer();
        DontDestroyOnLoad(player.gameObject);
        _foundInput = player;
		PlayerJoined(player);
    }
}
