using System;
using System.Linq;
using TMPro;
using UnityEngine;

namespace Speedrun
{
    [DefaultExecutionOrder(100),RequireComponent(typeof(TMP_Dropdown))]
    public class FillResolutions : MonoBehaviour
    {
        TMP_Dropdown dropdown;

        // Start is called before the first frame update
        void Awake()
        {
            //print(string.Join(", ", GetResolutions()));
            //print(string.Join(", ", Screen.resolutions));
            dropdown = GetComponent<TMP_Dropdown>();
            dropdown.options.Clear();
            dropdown.options.AddRange(GetResolutions().Select(res=>new TMP_Dropdown.OptionData($"{res.width} x {res.height}")).Reverse());
        }

        void Start()
        {
            UpdateValue();
        }

        private void UpdateValue()
        {
            var resolutions = GetResolutions().Reverse();
            var findIndex = resolutions.ToList().FindIndex(res => res.width == Screen.width && res.height == Screen.height);
            dropdown.value = findIndex;
            var res = "";
            for (var i = 0; i < dropdown.options.Count; i++)
            {
                if (i == findIndex)
                {
                    res += $", {dropdown.options[i].text} (selected)";
                }
                else
                {
                    res += $", {dropdown.options[i].text}";
                }
            }

            //Debug.LogError(res);
        }

        public void ChangeResolution(int index)
        {
            //Index is reversed, because I list the resolutions from biggest to smallest
            var resolutions = GetResolutions(); 
            index = resolutions.Length - 1 - index;
            var width = resolutions[index].width;
            var height = resolutions[index].height;
            Screen.SetResolution(width,height, Screen.fullScreenMode);
            //UpdateValue();
            SettingsManager.SaveResolution(index);
        }

        public static Resolution[] GetResolutions()
        {
            return Screen.resolutions.GroupBy(x => $"{x.width}x{x.height}").Select(x=>x.First()).ToArray();
        }
    }
}
