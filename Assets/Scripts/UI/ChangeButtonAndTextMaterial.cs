using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Speedrun
{
    [RequireComponent(typeof(Selectable))]
    public class ChangeButtonAndTextMaterial : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler
    {
        public TMP_Text text;
        [ColorUsage(true, true)] public Color normalColor;
        [ColorUsage(true, true)] public Color selectedColor;
        [ColorUsage(true, true)] public Color hoverColor;
        [ColorUsage(true, true)] public Color pressedColor;
        [ColorUsage(true, true)] public Color disabledColor;

        private Selectable selectable;
        private Graphic graphic;
        private Material buttonMat;
        private Material textMat;
        private bool hovering;
        private bool pressed;
        private Color targetColor;
        private Color fromColor;
        private Color currentColor;
        private float time = 0.0f;
        public float TweenDuration = 0.3f;
        public List<Graphic> additionalGraphics = new List<Graphic>();
        private List<Material> additionalGraphicsMaterials = new List<Material>();
        public UnityEvent<PointerEventData> OnClick = new UnityEvent<PointerEventData>();

        // Start is called before the first frame update
        void Awake()
        {
            selectable = GetComponent<Selectable>();
            if (selectable is Toggle tog)
            {
                graphic = tog.graphic;
            }
            else
            {
                graphic = GetComponent<Image>();
            }
            buttonMat = new Material(graphic.material);
            if (text)
            {
                textMat = new Material(text.fontMaterial);
                text.fontMaterial = textMat;
            }

            graphic.material = buttonMat;
            targetColor = buttonMat.GetColor("_Color");
            foreach (var additionalGraphic in additionalGraphics)
            {
                additionalGraphicsMaterials.Add(new Material(additionalGraphic.material));
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (selectable.interactable)
            {
                if (pressed)
                {
                    SetColor(pressedColor);
                }
                else if (EventSystem.current.currentSelectedGameObject == selectable.gameObject)
                {
                    SetColor(selectedColor);
                }
                else if (hovering)
                {
                    SetColor(hoverColor);
                }
                else
                {
                    SetColor(normalColor);
                }
            }
            else
            {
                SetColor(disabledColor);
            }

            currentColor = Color.Lerp(fromColor, targetColor, time / TweenDuration);
            buttonMat?.SetColor("_Color", currentColor);
            if (text)
                textMat.SetColor("_FaceColor", currentColor);
            foreach (var additionalGraphicsMaterial in additionalGraphicsMaterials)
            {
                buttonMat?.SetColor("_Color", currentColor);
            }
            time += Time.deltaTime;
        }

        private void SetColor(Color color)
        {
            if (color != targetColor)
            {
                time = 0;
                fromColor = currentColor;
                if (color == pressedColor)
                {
                    fromColor = color;
                }
            }

            targetColor = color;
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            hovering = true;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            hovering = false;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            pressed = true;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            pressed = false;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            OnClick.Invoke(eventData);
        }
    }
}
