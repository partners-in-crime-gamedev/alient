using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour
{
    public static bool IsSinglePlayerGame;
    public static bool StartedFromMenu { get; private set; } = false;

    [SerializeField] private string startGameScene;
    [SerializeField] private string creditsScene;
    public UnityEvent OnStartGame = new UnityEvent();
    [SerializeField] private PlayerInputManager inputManager;

    public void Awake()
    {
        IsSinglePlayerGame = false;
    }
    public void StartGame()
    {
        IsSinglePlayerGame = true;
        StartedFromMenu = true;
        OnStartGame.Invoke();
        GameState.Instance.ResetAbilities();
        SceneManager.LoadScene(startGameScene);
    }    
    
    public void StartCoopGame()
    {
        IsSinglePlayerGame = false;
        StartedFromMenu = true;
        OnStartGame.Invoke();
        GameState.Instance.ResetAbilities();
        SceneManager.LoadScene(startGameScene);
    }

    public void Credits()
    {
        SceneManager.LoadScene(creditsScene);
    }

    public void QuitGame()
    {
        Application.Quit();
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#endif
    }
}
