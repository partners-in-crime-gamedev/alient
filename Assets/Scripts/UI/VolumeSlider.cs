using System;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

namespace Speedrun
{
    [RequireComponent(typeof(Slider))]
    public class VolumeSlider : MonoBehaviour
    {
        private Slider slider;
        [SerializeField] private string m_PlayerPrefsString;
        // Start is called before the first frame update
        void Awake()
        {
            slider = GetComponent<Slider>();
            slider.SetValueWithoutNotify(PlayerPrefs.GetFloat(m_PlayerPrefsString, 1.0f));
        }
    }
}
