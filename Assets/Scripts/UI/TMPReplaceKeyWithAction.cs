using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;

/*
* Created by webik150
*/
[DefaultExecutionOrder(-100), RequireComponent(typeof(TMP_Text))]
public class TMPReplaceKeyWithAction : MonoBehaviour
{
    public InputActionAsset Action;
    public string ActionName;
    public string Key;
    public InputBinding.DisplayStringOptions MouseDisplayOptions;
    public InputBinding.DisplayStringOptions GamepadDisplayOptions;
    // Start is called before the first frame update
    void OnEnable()
    {
        var inputs = FindObjectsOfType<PlayerInput>();
        var anyGamepad = false;
        var anyMouse = false;
        string deviceMapping = "";
        foreach (var playerInput in inputs)
        {
            if (playerInput.devices.Any(device => device is Gamepad))
            {
                anyGamepad = true;
            }
            if (playerInput.devices.Any(device => device is Mouse))
            {
                anyMouse = true;
            }
        }

        deviceMapping = $"{(anyGamepad ? "<color=#0ff>R1</color>" : "")}{(anyGamepad && anyMouse ? " or " : "")}{(anyMouse ? "<color=#0ff>MMB</color>" : "")}";

        var text = GetComponent<TMP_Text>();

        //text.text = text.text.Replace(Key, deviceMapping);
        var gamePadText = "";
        var keyboardText = "";
        var action = Action.FindAction(ActionName, true);
        print(string.Join(" ", action.bindings.ToArray()));
        if (anyGamepad)
        {
            var bindingIndex = action.GetBindingIndex(InputBinding.MaskByGroup("Gamepad"));
            if (bindingIndex != -1)
                gamePadText = $"<color=#0ff>{action.GetBindingDisplayString(bindingIndex, GamepadDisplayOptions)}</color>";
        }
        if (anyMouse)
        {
            var bindingIndex = action.GetBindingIndex(InputBinding.MaskByGroups("Keyboard&Mouse"));
            if (bindingIndex != -1)
            {
                print(bindingIndex);
                if (action.bindings[bindingIndex].isComposite || action.bindings[bindingIndex].isPartOfComposite)
                {
                    var compositeText = action.bindings[bindingIndex].ToDisplayString(MouseDisplayOptions);
                    while (action.bindings[bindingIndex+1].isPartOfComposite || action.bindings[bindingIndex + 1].isComposite)
                    {
                        bindingIndex += 1;
                        compositeText += "|" + action.bindings[bindingIndex].ToDisplayString(MouseDisplayOptions);
                    }
                    keyboardText = $"<color=#0ff>{compositeText}</color>";
                }
                else
                {
                    keyboardText = $"<color=#0ff>{action.GetBindingDisplayString(bindingIndex, MouseDisplayOptions)}</color>";
                }
            }
        }
        text.text = text.text.Replace(Key, $"{gamePadText}{(anyGamepad && anyMouse ? " or " : "")}{keyboardText}");
    }

    // Update is called once per frame
    void Update()
    {

    }
}
