using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;


public class MouseEvents : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private FMOD.Studio.EventInstance sfx;
    private PauseManager pauseManager;

    void Start()
    {
        pauseManager = FindObjectOfType<PauseManager>();
        pauseManager?.OnUnPauseEvent.AddListener(StopHoverSFX);
    }

    public void StopHoverSFX() 
    {
        sfx.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        sfx.release();
    }

    void PlayHoverSFX() 
    {
        sfx = FMODUnity.RuntimeManager.CreateInstance("event:/MENU/hover");
        sfx.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject.transform));
        sfx.start();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        PlayHoverSFX();

    }
 
    public void OnPointerExit(PointerEventData eventData)
    {
        StopHoverSFX();

    }
}
