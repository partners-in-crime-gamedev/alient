using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
* Created by webik150
*/
[RequireComponent(typeof(Button))]
public class EnableButtonOnInputsReady : MonoBehaviour
{
    Button m_Button;
    [SerializeField] private List<PlayerInputSelection> m_Inputs;
    // Start is called before the first frame update
    void Start()
    {
        m_Button = GetComponent<Button>();
    }

    // Update is called once per frame
    void Update()
    {
        bool ready = true;
        foreach (PlayerInputSelection input in m_Inputs)
        {
            if (!input.InputDeviceFound)
            {
                ready = false;
                break;
            }
        }

        m_Button.interactable = ready;
    }
}
