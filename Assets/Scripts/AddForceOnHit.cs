using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
* Created by webik150
*/

public class AddForceOnHit : MonoBehaviour
{
    public void GetHit(object sender, OnHitEventArgs e)
    {

        if (e.Origin != Vector3.zero)
        {
            foreach (var rigidbody in GetComponentsInChildren<Rigidbody>())
            {
                rigidbody.isKinematic = false;
                rigidbody.AddExplosionForce(e.Force, e.Origin, 10);
            }
        }
    }
}
