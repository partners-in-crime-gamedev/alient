using UnityEngine;

namespace Speedrun
{
    public class DestroyAfterTime : MonoBehaviour
    {
        public float time = 1.0f;

        public GameObject Particles;

        // Start is called before the first frame update
        private void Start()
        {
            Invoke("DestroyThis", time);
        }

        private void DestroyThis()
        {
            Destroy(gameObject);
            if (Particles)
            {
                GameObject o = Instantiate(Particles);
                o.transform.position = transform.position;
                o.transform.rotation = transform.rotation;
            }
        }
    }
}
