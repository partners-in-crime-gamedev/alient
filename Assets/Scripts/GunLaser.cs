using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
* Created by webik150
*/
[RequireComponent(typeof(LineRenderer))]
public class GunLaser : MonoBehaviour
{
    private LineRenderer trail;
    private PlayerOutlineData playerOutlineData;

    [SerializeField]private float maxDistance = 9f;
    [SerializeField] private LayerMask layerMask;
    [SerializeField] private Gun gun;

    private float currentDistance = 0f;

    // Start is called before the first frame update
    void Awake()
    {
        trail = GetComponent<LineRenderer>();
        playerOutlineData = GetComponentInParent<PlayerOutlineData>();
    }

    // Update is called once per frame
    void Update()
    {
        if (gun)
        {
            if (gun.NoObstacle())
            {
                trail.enabled = true;
            }
            else
            {
                trail.enabled = false;
            }
        }
        if (trail)
        {
            if (playerOutlineData)
            {
                trail.startColor = playerOutlineData.playerRenderer.material.color;
                trail.endColor = trail.startColor;
            }

            if(Physics.Raycast(transform.position, transform.right, out RaycastHit hit, maxDistance, layerMask, QueryTriggerInteraction.Ignore))
            {
                trail.SetPosition(1, new Vector3(hit.distance, 0, 0));
                currentDistance = hit.distance;
            }
            else
            {
                trail.SetPosition(1, new Vector3(maxDistance, 0, 0));
                currentDistance = maxDistance;
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta/2;
        Gizmos.DrawRay(transform.position, transform.right);
    }


}
