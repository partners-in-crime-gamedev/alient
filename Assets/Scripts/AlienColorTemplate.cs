using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
* Created by webik150
*/

[RequireComponent(typeof(Button))]
public class AlienColorTemplate : MonoBehaviour
{
    private Button _button;
    private PlayerInputSelection _playerInputSelection;
    public Image ColorPreview;
    public Color Color;
    // Start is called before the first frame update
    void OnEnable()
    {
        _button = GetComponent<Button>();
        _button.onClick.AddListener(Select);
        _playerInputSelection = GetComponentInParent<PlayerInputSelection>(true);
    }

    void OnDisable()
    {
        _button.onClick.RemoveListener(Select);
    }

    public void Select()
    {
        _playerInputSelection?.SetColor(Color);
    }

    // Update is called once per frame
    void Update()
    {
        if (ColorPreview)
        {
            ColorPreview.color = Color;
        }
    }
}
