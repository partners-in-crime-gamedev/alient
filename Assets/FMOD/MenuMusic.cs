using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuMusic : MonoBehaviour
{

private FMOD.Studio.EventInstance instance;
	
	private bool onceStarted = false;

	void Start() {
		onceStarted = false;
		Debug.Log("start music");
	}

	private static MenuMusic _THIS;
	public static MenuMusic THIS
	{
		get
		{
			if (_THIS != null) return _THIS;
			_THIS = FindObjectOfType<MenuMusic>();
			if (_THIS == null)
			{
				GameObject go = new GameObject();

				go.name = nameof(MenuMusic);
				_THIS = go.AddComponent<MenuMusic>();
			}
			GameObject.DontDestroyOnLoad(_THIS);
			return _THIS;
		}
	}

	void Awake()
    {
		FMOD.Studio.PLAYBACK_STATE state;
		instance.getPlaybackState(out state);
		Debug.Log(state);

		if (!onceStarted || state != FMOD.Studio.PLAYBACK_STATE.STOPPED)
		{
		instance.getPlaybackState(out state);
		Debug.Log(state);
		Debug.Log("play in AWKAE");
		MenuMusic.THIS.PlayMusic();
		onceStarted = true;

		}
    }

	public void PlayMusic()
    {
		instance = FMODUnity.RuntimeManager.CreateInstance("event:/MENU/menu_music");
		instance.start();
		instance.release();
	}

	// Update is called once per frame
	void Update()
	{
	
	}
	
}
