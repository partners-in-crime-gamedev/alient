using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioSettings : MonoBehaviour
{
	FMOD.Studio.Bus Master;
	FMOD.Studio.Bus Music;
	FMOD.Studio.Bus SFX;

	FMOD.Studio.EventInstance SFXTestEvent;

	[SerializeField] float MasterVol = 1.0f;
	[SerializeField] float MusicVol = 0.5f;
	[SerializeField] float SFXVol = 0.5f;

	void Awake()
	{
		//DontDestroyOnUnload(this);
		Music = FMODUnity.RuntimeManager.GetBus("bus:/master/music_grp");
		SFX = FMODUnity.RuntimeManager.GetBus("bus:/master/sfx_grp");
		Master = FMODUnity.RuntimeManager.GetBus("bus:/master");
	}

	void PlaySFX(float vol) 
	{	
		FMOD.Studio.PLAYBACK_STATE PbState;
		SFXTestEvent.getPlaybackState(out PbState);
		if (PbState != FMOD.Studio.PLAYBACK_STATE.PLAYING)
		{
			SFXTestEvent.setVolume(vol);
			SFXTestEvent.start();
		}
	}


	void Update()
	{
		Music.setVolume(MusicVol);
		SFX.setVolume(SFXVol);
		Master.setVolume(MasterVol);
	}

	public void MasterVolumeLevel(float newMasterVol)
	{
		MasterVol = newMasterVol;
	}

	public void MusicVolumeLevel(float newMusicVol)
	{
		MusicVol = newMusicVol;
	}

	public void SFXVolumeLevel(float newSFXVol)
	{
		SFXVol = newSFXVol;
	}
}

